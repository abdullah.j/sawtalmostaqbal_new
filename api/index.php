<?php
/**
 * Main API
 *
 * @package Sawt al moustakbal
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 *
 */

/*Load WordPress start*/
require_once '../wp-load.php';
/*Load WordPress start*/

/*Load Defs start*/
require_once "common/definitions.php";
/*Load Defs end*/

/*Load Autoload start*/
require_once 'vendor/autoload.php';
/*Load Autoload end*/

/*Load JWT php library start*/

use \Firebase\JWT\JWT;

/*Load JWT php library end*/

/*Methods start*/

/*Load/set apiRouting start*/
require_once "methods/apiRouting.php";
$router = new AltoRouter();
$router->setBasePath( '/api' );
/*Load/set apiRouting end*/

/*Load Helpers start*/
require_once 'methods/helpers.php';
/*Load Helpers end*/

/*Load Routes start*/
require_once 'methods/routes.php';
/*Load Routes end*/

/*Methods end*/

/**
 * Return encoded JSON.
 *
 * @param $code int
 * @param $message string
 *
 * @return json
 */
function json_response( $code = 200, $data = [] ) {
	// clear the old headers
	header_remove();

	// Set the actual code
	http_response_code( $code );

	// Set the header to make sure cache is forced
	header( "Cache-Control: no-transform,public,max-age=300,s-maxage=900" );

	// Set the header to make sure content return in json format
	header( 'Content-Type: text/json; charset=utf-8' );

	// Predefined status array
	$status = array(
		200 => '200 OK',
		400 => '400 Bad Request',
		422 => 'Unprocessable Entity',
		500 => '500 Internal Server Error',
	);
	header( 'Status: ' . $status[ $code ] );

	// Return the encoded json
	return json_encode( $data );
}

// Match current request url
$match = $router->match();

// Call closure or throw 404 status
if ( $match && is_callable( $match['target'] ) ) {

	//get JWT token
	$returned_token = isset( $_POST['returned_token'] ) ? $_POST['returned_token'] : '';

	if ( $returned_token ) {

		try {

			//decode JWT token

			$secretKey = base64_decode( JWT_SECRET );

			$decoded = JWT::decode( $returned_token, $secretKey, array( 'HS512' ) );

			if ( $decoded ) {

				call_user_func_array( $match['target'], $match['params'] );

			} else if ( $decoded->exp < time() ) {

				header( 'HTTP/1.0 401 Unauthorized' );
				$data = array(
					'status'  => false,
					'message' => 'JWT token expired',
				);
				echo json_response( 401, $data );
				exit();

			} else {
				header( 'HTTP/1.0 401 Unauthorized' );
				$data = array(
					'status'  => false,
					'message' => 'Could not decode the JWT token.',
				);
				echo json_response( 401, $data );
				exit();
			}

		} catch ( \Exception $e ) {

			header( 'HTTP/1.0 401 Unauthorized' );
			$data = array(
				'status'  => false,
				'message' => 'An exception has occurred',
				'details' => $e->getMessage()
			);
			echo json_response( 401, $data );
			exit();

		}

	} else {
		// Get creds
		$access_username = isset( $_POST['access_username'] ) ? $_POST['access_username'] : '';
		$access_pass     = isset( $_POST['access_pass'] ) ? $_POST['access_pass'] : '';

		if ( empty( $access_username ) || empty( $access_pass ) ) {
			header( 'HTTP/1.0 401 Unauthorized' );
			$data = array(
				'status'  => false,
				'message' => 'Auth username and password are required',
			);
			echo json_response( 401, $data );
			exit();
		}

		$credentials  = [
			'user_login'    => $access_username,
			'user_password' => $access_pass,
		];
		$authenticate = wp_signon( $credentials, false );

		if ( ! is_wp_error( $authenticate ) ) {

			$tokenId  = base64_encode( mcrypt_create_iv( 32 ) );
			$issuedAt = time();
			//    $expire = $issuedAt + 86400;
			$expire = $issuedAt + 600;

			$JWT_data = [
				'iat'  => $issuedAt,
				'jti'  => $tokenId,
				'iss'  => SERVER_NAME,
				'aud'  => SERVER_NAME,
				'exp'  => $expire,
				'data' => [
					'user_id' => 2,
				],
			];

			$secretKey = base64_decode( JWT_SECRET );

			$JWT = JWT::encode(
				$JWT_data,
				$secretKey,
				'HS512'
			);

			date_default_timezone_set( 'Asia/Beirut' );
			$readable_iss_at = date( 'm/d/Y h:i:s a', $issuedAt );
			$readable_expire = date( 'm/d/Y h:i:s a', $expire );

			//Send api response data
			$data = array(
				'status'         => true,
				'issued_at'      => $readable_iss_at,
				'expire'         => $readable_expire,
				'returned_token' => $JWT,
			);
			echo json_response( 200, $data );
			exit();

		} else {
			header( 'HTTP/1.0 401 Unauthorized' );
			$data = array(
				'status'  => false,
				'message' => 'Invalid auth access',
			);
			echo json_response( 401, $data );
			exit();
		}

	}

} else {
	// no route was matched
	header( $_SERVER["SERVER_PROTOCOL"] . '404 Not Found' );
}
