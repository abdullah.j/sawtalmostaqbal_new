<?php

/**
 * GET Methods
 *
 * @package Sawt al moustakbal
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer Jean El Khoury
 *
 */
/* Load GET Routes start */
require_once 'routes/guest.content.php';
require_once 'routes/guest.ads.php';
require_once 'routes/guest.email.php';
require_once 'routes/guest.programs.php';
require_once 'routes/guest.programs.single.php';
require_once 'routes/guest.programs.featured.php';
require_once 'routes/guest.programs.homepage.php';
require_once 'routes/guest.episodes.homepage.php';
require_once 'routes/guest.episodes.single.php';
require_once 'routes/guest.episodes.php';
require_once 'routes/guest.livestream.php';
require_once 'routes/guest.homepage.php';
require_once 'routes/guest.poll.php';
/*Load GET Routes end*/