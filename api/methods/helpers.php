<?php

/**
 * Helpers Methods
 *
 * @package Sawt al moustakbal  
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer Jean El Khoury
 */
function sem_get_programs($post_id) {
    $post_title = get_the_title($post_id);
    $post_link = get_the_permalink($post_id);

    $field_image_full = get_the_post_thumbnail_url($post_id, 'full');
    $field_image_resized = get_the_post_thumbnail_url($post_id, 'homepage_programs');

    $content_post = get_post($post_id);
    $field_desc = $content_post->post_content;
    $field_desc = apply_filters('the_content', $field_desc);
    $field_desc = str_replace(']]>', ']]&gt;', $field_desc);

    $program_logo_id = get_field('program_logo', $post_id);
    $program_logo = wp_get_attachment_image_src($program_logo_id, 'homepage_slider_prg_logo')[0];
    $program_time = get_field('program_time',$post_id);

    $program_on_homepage = get_field('program_on_homepage', $post_id);
    $program_is_featured = get_field('featured_program', $post_id);
    $program_audio_or_video = get_field('program_audio_or_video', $post_id);

    $temp = [];
    $temp['post_id'] = (string) $post_id;
    $temp['post_title'] = $post_title;
    $temp['post_link'] = $post_link;
    $temp['field_desc'] = $field_desc;
    $temp['field_image_full'] = $field_image_full;
    $temp['field_image_resized'] = $field_image_resized;
    $temp['program_logo'] = $program_logo;
    $temp['program_time'] = $program_time;
    $temp['program_on_homepage'] = $program_on_homepage;
    $temp['program_is_featured'] = $program_is_featured;
    $temp['program_audio_or_video'] = $program_audio_or_video;

    return $temp;
}

function sem_get_episodes($post_id) {
    $post_title = get_the_title($post_id);
    $post_link = get_the_permalink($post_id);

    $field_image_full = get_the_post_thumbnail_url($post_id, 'full');
    $field_image_resized = get_the_post_thumbnail_url($post_id, 'homepage_small_episode_img');
    $field_image_square = get_the_post_thumbnail_url($post_id, 'thumbnail');

    $episode_program = get_field('episode_program', $post_id);
    $episode_program_title = get_the_title($episode_program);
    $episode_on_homepage = get_field('episode_on_homepage', $post_id);
    $episode_soundcloud = get_field('soundcloud_iframe_code', $post_id);

    if ($episode_soundcloud) {
        preg_match('/src="(.*?)"/', $episode_soundcloud, $match);

        $episode_soundcloud_src = $match[1];
    }else{
        $episode_soundcloud_src = '';
    }

    $episode_youtube = get_field('episode_youtube_id', $post_id);
    $video_or_audio = get_field('video_or_audio', $post_id);

    $episode_program_image = get_the_post_thumbnail_url($episode_program, 'homepage_programs');

    $temp = [];
    $temp['post_id'] = (string) $post_id;
    $temp['post_title'] = $post_title;
    $temp['post_link'] = $post_link;
    $temp['episode_program'] = $episode_program_title;
    $temp['episode_program_image'] = $episode_program_image;
    $temp['episode_on_homepage'] = $episode_on_homepage;
    $temp['episode_soundcloud'] = $episode_soundcloud;
    $temp['episode_soundcloud_src'] = $episode_soundcloud_src;
    $temp['episode_youtube'] = $episode_youtube;
    $temp['episode_full_image'] = $field_image_full;
    $temp['episode_resized_image'] = $field_image_resized;
    $temp['episode_square_image'] = $field_image_square;
    $temp['episode_video_or_audio'] = $video_or_audio;

    return $temp;
}
