<?php

/**
 * @Route /api/guest/content
 * @Type GET
 * @Variables:
 *
 * +returned_token: Valid generated JWT access token
 * +type*: requested content
 * ++type=1: About Us
 * ++type=2: Social URLs
 * ++type=3: Contact Info
 * ++type=4: Privacy Policy
 * ++type=5: Google Analytics
 *
 * @package Sawt al moustakbal  
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer Jean El Khoury
 *
 */
$router->map('POST', '/guest/content', function () {

    extract($_POST);

    //GET sent vars
    $type = isset($type) ? $type : "";

    $errors = 0;

    if (!$type) {

        $errors = 1;

        //Send api response data
        $data = array(
            'status' => false,
            'error_code' => 'gc_001',
            'error_type' => 'missing_vars',
            'message' => 'type parameter missing',
        );


        echo json_response(200, $data);
        exit();
    }


    if (!$errors) {

        switch ($type):
            case 1:
                $about_title = get_field('title', 'option');
                $about_content = get_field('content', 'option');
                $type_t = "About Us";
                $data = array(
                    'status' => true,
                    'message' => 'Content',
                    'type' => $type_t,
                    'type_id' => $type,
                    'data' => array(
                        'about_title' => $about_title,
                        'about_content' => $about_content
                    )
                );
                break;

            case 2:
                $facebook_url = get_field('facebook_link', 'option');
                $youtube_url = get_field('youtube_link', 'option');
                $instagram_url = get_field('instagram_link', 'option');
                $soundcloud_url = get_field('soundcloud_link', 'option');
                $type_t = "Social Media URLs";
                $data = array(
                    'status' => true,
                    'message' => 'Content',
                    'type' => $type_t,
                    'type_id' => $type,
                    'data' => array(
                        'facebook' => $facebook_url,
                        'youtube' => $youtube_url,
                        'instagram' => $instagram_url,
                        'soundcloud' => $soundcloud_url
                    )
                );
                break;

            case 3:
                $address = get_field('address', 'option');
                $phone_number = get_field('phone_number', 'option');
                $email = get_field('email', 'option');
                $type_t = "Contact Us Info";
                $data = array(
                    'status' => true,
                    'message' => 'Content',
                    'type' => $type_t,
                    'type_id' => $type,
                    'data' => array(
                        'address' => $address,
                        'phone_number' => $phone_number,
                        'email' => $email
                    )
                );
                break;

            case 4:
                $text = get_field('privacy_policy_text', 'option');                
                $type_t = "Privacy Policy";
                $data = array(
                    'status' => true,
                    'message' => 'Content',
                    'type' => $type_t,
                    'type_id' => $type,
                    'data' => array(
                        'text' => $text
                    )
                );
                break;

            case 5:
                $text = get_field('google_analytics_script', 'option');
                $text = str_replace(array("\n", "\t", "\r"), '', $text);
                $type_t = "Google Analytics script";
                $data = array(
                    'status' => true,
                    'message' => 'Content',
                    'type' => $type_t,
                    'type_id' => $type,
                    'data' => array(
                        'text' => $text
                    )
                );
                break;

            default:

                //Send api response data
                $data = array(
                    'status' => false,
                    'error_code' => 'gc_002',
                    'error_type' => 'specific',
                    'message' => 'invalid type parameter',
                );

                echo json_response(200, $data);
                exit();

        endswitch;

        //Send api response data
        echo json_response(200, $data);
    } else {

        //Send api response data
        $data = array(
            'status' => false,
            'error_code' => 'gc_003',
            'error_type' => 'general',
            'message' => 'An unknown error occurred',
        );

        echo json_response(200, $data);
        exit();
    }
}, "guest/content");
