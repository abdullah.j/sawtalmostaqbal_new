<?php

/**
 * @Route /api/guest/homepage
 * @Type GET
 * @Variables:
 *
 * +returned_token: Valid generated JWT access token
 *
 * @package Sawt al moustakbal  
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer Marc bou sleiman <http://marcbousleiman.com>
 *
 */
$router->map('POST', '/guest/homepage', function () {

    //Timezone set
    date_default_timezone_set('Asia/Beirut');

    //GET sent vars
    extract($_POST);

    $errors = 0;

    //Vars init
    $data = [];
    $top_programs = [];
    $programs = [];
    $homepage_ads = [];
    $bottom_episodes = [];
    $count = 0;

//    query args top programs
    if (have_rows('homepage_slider', 'option')):
        while (have_rows('homepage_slider', 'option')) : the_row();
            $slider_program = get_sub_field('slide_program', 'option');
            $count++;
            $temp = sem_get_programs($slider_program);
            $top_programs[] = $temp;
        endwhile;
    endif;

//    homepage ads
    $ad_type_toggle_homepage = get_field('ad_type_toggle_homepage', 'options');
    if ($ad_type_toggle_homepage == 0):
        $image_ad_homepage = get_field('image_ad_homepage', 'options');
        $android_link_homepage = get_field('image_link_android_homepage', 'options');
        $ios_link_homepage = get_field('image_link_ios_homepage', 'options');

        $data_homepage = array(
            'type_homepage' => 'Image Ad',
            'image' => $image_ad_homepage,
            'android_link' => $android_link_homepage,
            'ios_link' => $ios_link_homepage
        );
    else:
        $android_admob_homepage = get_field('ad_unit_id_android_homepage', 'options');
        $ios_admob_homepage = get_field('ad_unit_id_ios_homepage', 'options');

        $data_homepage = array(
            'type_homepage' => 'Admod',
            'android' => $android_admob_homepage,
            'ios' => $ios_admob_homepage
        );
    endif;


    //Query args middle posts
    $args = array(
        'posts_per_page' => 8,
        'post_type' => 'programs',
        'order' => 'asc',
        'post_status' => 'publish'
    );

    //Query
    $query = new WP_Query($args);
    if ($query->have_posts()):
        while ($query->have_posts()):
            $query->the_post();

            $count++;
            $post_id = get_the_ID();
            $temp = sem_get_programs($post_id);
            $programs[] = $temp;

        endwhile;
    endif;

//    query args for bottom episodes
    $args_episodes = array(
        'post_type' => 'programs_episodes',
        'posts_per_page' => 6,
        'order' => 'DESC',
        'post_status' => 'publish',
        'meta_query' => array(
            array(
                'key' => 'episode_on_homepage',
                'compare' => '==',
                'value' => '1'
            )
        )
    );

    $first_episodes = new WP_Query($args_episodes);

    if ($first_episodes->have_posts()):
        while ($first_episodes->have_posts()):
            $first_episodes->the_post();
            $count++;
            $post_id = get_the_ID();
            $temp = sem_get_episodes($post_id);
            $bottom_episodes[] = $temp;
        endwhile;
    endif;


    if (empty($programs) && empty($top_programs) && empty($bottom_episodes)) {

        //Send api response data
        $data = array(
            'status' => true,
            'message' => 'No programs/episodes found',
        );
        echo json_response(200, $data);
    } else {

        //Send api response data
        $data = array(
            'status' => true,
            'message' => 'Homepage layout',
            'data' => array(
                'count' => $count,
                'top_posts' => $top_programs,
                'middle_posts' => $programs,
                'homepage_ad' => $data_homepage,
                'bottom_posts' => $bottom_episodes,
            ),
        );

        echo json_response(200, $data);
    }
}, "guest/homepage");
