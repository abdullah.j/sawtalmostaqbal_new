<?php

/**
 * @Route /api/guest/episodes
 * @Type GET
 * @Variables:
 *
 * +returned_token: Valid generated JWT access token
 * +posts_per_page: posts per page
 * +current_page: current pagination page
 * +program_id: program to retrieve its episodes
 *
 * @package Sawt al moustakbal  
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer Jean El Khoury
 *
 */
$router->map('POST', '/guest/episodes', function () {

    //Timezone set
    date_default_timezone_set('Asia/Beirut');

    //GET sent vars
    extract($_POST);

    $posts_per_page = isset($posts_per_page) ? $posts_per_page : 5;
    $current_page = isset($current_page) ? $current_page : 1;
    $program_id = isset($program_id) ? $program_id : '';

    $errors = 0;

    if (!get_post_status($program_id)) {
        $errors = 1;

        //Send api response data
        $data = array(
            'status' => false,
            'error_code' => 'gues_001',
            'error_type' => 'missing_vars',
            'message' => 'invalid program_id',
        );

        echo json_response(200, $data);
        exit();
    }

    if (!$program_id) {

        $errors = 1;

        //Send api response data
        $data = array(
            'status' => false,
            'error_code' => 'gues_002',
            'error_type' => 'missing_vars',
            'message' => 'program_id parameter missing',
        );

        echo json_response(200, $data);
        exit();
    }

    if (!$errors) {

        //Vars init
        $data = [];
        $episodes = [];
        $count = 0;

        //Query args
        $offset = ($posts_per_page * $current_page) - $posts_per_page;
        $args = array(
            'paged' => $current_page,
            'offset' => $offset,
            'posts_per_page' => $posts_per_page,
            'post_type' => 'programs_episodes',
            'order' => 'DESC',
            'meta_key' => 'episode_program',
            'meta_value' => $program_id
        );

        //Query
        $query = new WP_Query($args);
        if ($query->have_posts()):
            while ($query->have_posts()):
                $query->the_post();

                $count++;
                $post_id = get_the_ID();
                $temp = sem_get_episodes($post_id);
                $episodes[] = $temp;

            endwhile;
        endif;

        if (empty($episodes)) {

            //Send api response data
            $data = array(
                'status' => true,
                'message' => 'No episodes found',
            );
            echo json_response(200, $data);
        } else {

            //Send api response data
            $data = array(
                'status' => true,
                'message' => 'Episodes',
                'data' => array(
                    'posts_per_page' => $posts_per_page,
                    'current_page' => $current_page,
                    'offset' => $offset,
                    'count' => $count,
                    'posts' => $episodes,
                ),
            );

            echo json_response(200, $data);
        }
    } else {
        //Send api response data
        $data = array(
            'status' => false,
            'error_code' => 'gues_003',
            'error_type' => 'general',
            'message' => 'An unknown error occurred',
        );

        echo json_response(200, $data);
        exit();
    }
}, "guest/episodes");
