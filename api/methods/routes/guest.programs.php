<?php

/**
 * @Route /api/guest/programs
 * @Type GET
 * @Variables:
 *
 * +returned_token: Valid generated JWT access token
 * +posts_per_page: posts per page
 * +current_page: current pagination page
 *
 * @package Sawt al moustakbal  
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer Jean El Khoury
 *
 */
$router->map('POST', '/guest/programs', function () {

    //Timezone set
    date_default_timezone_set('Asia/Beirut');

    //GET sent vars
    extract($_POST);

    $posts_per_page = isset($posts_per_page) ? $posts_per_page : 6;
    $current_page = isset($current_page) ? $current_page : 1;

    $errors = 0;

    //Vars init
    $data = [];
    $programs = [];
    $count = 0;

    //Query args
    $offset = ($posts_per_page * $current_page) - $posts_per_page;
    $args = array(
        'paged' => $current_page,
        'offset' => $offset,
        'posts_per_page' => $posts_per_page,
        'post_type' => 'programs',
        'order' => 'asc',
    );

    //Query
    $query = new WP_Query($args);
    if ($query->have_posts()):
        while ($query->have_posts()):
            $query->the_post();

            $count++;
            $post_id = get_the_ID();
            $temp = sem_get_programs($post_id);
            $programs[] = $temp;

        endwhile;
    endif;
    
    $maximum_programs_pages = $query->max_num_pages;

        if (($current_page + 1) > $maximum_programs_pages) {
            $program_is_last_page = true;
        } else {
            $program_is_last_page = false;
        }

    if (empty($programs)) {

        //Send api response data
        $data = array(
            'status' => true,
            'message' => 'No programs found',
        );
        echo json_response(200, $data);
    } else {

        //Send api response data
        $data = array(
            'status' => true,
            'message' => 'Programs',
            'data' => array(
                'posts_per_page' => $posts_per_page,
                'current_page' => $current_page,
                'offset' => $offset,
                'count' => $count,
                'posts' => $programs,
                'program_is_last_page' => $program_is_last_page
            ),
        );

        echo json_response(200, $data);
    }
}, "guest/programs");
