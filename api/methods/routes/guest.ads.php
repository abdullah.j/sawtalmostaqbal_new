<?php

/**
 * @Route /api/guest/ads
 * @Type GET
 * @Variables:
 *
 * +returned_token: Valid generated JWT access token
 *
 * @package Sawt al moustakbal  
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer Jean El Khoury
 *
 */
$router->map('POST', '/guest/ads', function () {

    $errors = 0;

    if (!$errors) {

        $ad_type_toggle_homepage = get_field('ad_type_toggle_homepage', 'options');
        $data_homepage = [];

        if ($ad_type_toggle_homepage == 0):
                $image_ad_homepage = get_field('image_ad_homepage', 'options');
            $android_link_homepage = get_field('image_link_android_homepage', 'options');
            $ios_link_homepage = get_field('image_link_ios_homepage', 'options');

            $data_homepage = array(
                'type_homepage' => 'Image Ad',
                'image' => $image_ad_homepage,
                'android_link' => $android_link_homepage,
                'ios_link' => $ios_link_homepage
            );
        else:
            $android_admob_homepage = get_field('ad_unit_id_android_homepage', 'options');
            $ios_admob_homepage = get_field('ad_unit_id_ios_homepage', 'options');

            $data_homepage = array(
                'type_homepage' => 'Admod',
                'android' => $android_admob_homepage,
                'ios' => $ios_admob_homepage
            );
        endif;

        $data = array(
            'status' => true,
            'message' => 'Mobile Ads',
            'data' => array(
                'homepage_ad' => $data_homepage,
            )
        );

        //Send api response data
        echo json_response(200, $data);
    } else {

        //Send api response data
        $data = array(
            'status' => false,
            'error_code' => 'gc_001',
            'error_type' => 'general',
            'message' => 'An unknown error occurred',
        );

        echo json_response(200, $data);
        exit();
    }
}, "guest/ads");
