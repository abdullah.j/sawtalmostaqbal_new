<?php

/**
 * @Route /api/guest/episodes/single
 * @Type GET
 * @Variables:
 *
 * +returned_token: Valid generated JWT access token
 * +posts_per_page: posts per page
 * +current_page: current pagination page
 * +episode_id: episode id to get its info and related episodes
 *
 * @package Sawt al moustakbal  
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer Jean El Khoury
 *
 */
$router->map('POST', '/guest/episodes/single', function () {

    //Timezone set
    date_default_timezone_set('Asia/Beirut');

    //GET sent vars
    extract($_POST);

    $posts_per_page = isset($posts_per_page) ? $posts_per_page : 6;
    $current_page = isset($current_page) ? $current_page : 1;
    $episode_id = isset($episode_id) ? $episode_id : '';

    $errors = 0;

    if (!get_post_status($episode_id)) {
        $errors = 1;

        //Send api response data
        $data = array(
            'status' => false,
            'error_code' => 'gues_001',
            'error_type' => 'missing_vars',
            'message' => 'invalid episode_id',
        );

        echo json_response(200, $data);
        exit();
    }

    if (!$episode_id) {

        $errors = 1;

        //Send api response data
        $data = array(
            'status' => false,
            'error_code' => 'gues_002',
            'error_type' => 'missing_vars',
            'message' => 'episode_id parameter missing',
        );

        echo json_response(200, $data);
        exit();
    }

    if (!$errors) {

        //Vars init
        $data = [];
        $episodes = [];
        $count = 0;

        $episode_parent_program = get_field('episode_program', $episode_id);
        $parent_video_or_audio = get_field('program_audio_or_video', $episode_parent_program);

        //single episode query
        $args_episode = array(
            'post_type' => 'programs_episodes',
            'p' => $episode_id
        );

        //Query
        $query_episode = new WP_Query($args_episode);
        if ($query_episode->have_posts()):
            while ($query_episode->have_posts()):
                $query_episode->the_post();

                $count++;
                $post_id = get_the_ID();
                $temp = sem_get_episodes($post_id);
                $episodes[] = $temp;

            endwhile;
        endif;

        //Query args
        $offset = ($posts_per_page * $current_page) - $posts_per_page;
        $args = array(
            'paged' => $current_page,
            'offset' => $offset,
            'posts_per_page' => $posts_per_page,
            'post_type' => 'programs_episodes',
            'order' => 'DESC',
            'meta_key' => 'episode_program',
            'meta_value' => $episode_parent_program,
            'post__not_in' => array($episode_id)
        );

        //Query
        $query = new WP_Query($args);
        if ($query->have_posts()):
            while ($query->have_posts()):
                $query->the_post();

                $count++;
                $post_id = get_the_ID();
                $temp = sem_get_episodes($post_id);
                $episodes[] = $temp;

            endwhile;
        endif;

        $maximum_episodes_pages = $query->max_num_pages;

        if (($current_page + 1) > $maximum_episodes_pages) {
            $episodes_is_last_page = true;
        } else {
            $episodes_is_last_page = false;
        }

        if (empty($episodes)) {

            //Send api response data
            $data = array(
                'status' => true,
                'message' => 'No episodes found',
            );
            echo json_response(200, $data);
        } else {

            //Send api response data
            $data = array(
                'status' => true,
                'message' => 'Episodes',
                'data' => array(
                    'posts_per_page' => $posts_per_page,
                    'current_page' => $current_page,
                    'offset' => $offset,
                    'count' => $count,
                    'posts' => $episodes,
                    'parent_video_or_audio' => $parent_video_or_audio,
                    'episodes_last_page' => $episodes_is_last_page
                ),
            );

            echo json_response(200, $data);
        }
    } else {
        //Send api response data
        $data = array(
            'status' => false,
            'error_code' => 'gues_003',
            'error_type' => 'general',
            'message' => 'An unknown error occurred',
        );

        echo json_response(200, $data);
        exit();
    }
}, "guest/episodes/single");
