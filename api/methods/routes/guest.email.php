<?php

/**
 * @Route /api/guest/email
 * @Type GET
 * @Variables:
 *
 * +returned_token: Valid generated JWT access token
 * +email: Sender email
 * +full_name: Sender's full name
 * +phone_number: Sender's phone number
 * +message: Message to be sent
 * +remote_ip: Remote IP of sender
 * +user_agent: User Agent of sender
 * +date: Date of sending the email (Format: March 14, 2018)
 * +time: Time of sending the email (Format: 1:15 pm)
 *
 * @package Sawt al mustaqbal  
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer Jean El Khoury
 *
 */
$router->map('POST', '/guest/email', function () {

    extract($_POST);

    //GET sent variables
    $email = isset($email) ? $email : '';
    $subject = 'Sawt al Mostaqbal App - Contact form';
    $full_name = isset($full_name) ? $full_name : '';
    $phone_number = isset($phone_number) ? $phone_number : '-';
    $message = isset($message) ? $message : '';
    $remote_ip = isset($remote_ip) ? $remote_ip : '';
    $user_agent = isset($user_agent) ? $user_agent : '';
    $date = isset($date) ? $date : '';
    $time = isset($time) ? $time : '';

    $error = 0;

    if (!$email):
        $error = 1;

        //Send api response data
        $data = array(
            'status' => false,
            'error_code' => 'gc_001',
            'error_type' => 'missing_vars',
            'message' => 'email parameter missing',
        );


        echo json_response(200, $data);
        exit();
    endif;

    if (!$full_name):
        $error = 2;

        //Send api response data
        $data = array(
            'status' => false,
            'error_code' => 'gc_002',
            'error_type' => 'missing_vars',
            'message' => 'full name parameter missing',
        );


        echo json_response(200, $data);
        exit();
    endif;

    if (!$message):
        $error = 3;

        //Send api response data
        $data = array(
            'status' => false,
            'error_code' => 'gc_003',
            'error_type' => 'missing_vars',
            'message' => 'message parameter missing',
        );


        echo json_response(200, $data);
        exit();
    endif;


    if (!$error):
        $headers = array('From:' => $email);
        $mailResult = wp_mail('marco.bousleiman@gmail.com', $subject, $message, $headers);
        $data = array(
            'status' => true,
            'message' => 'Email Status',
            'data' => array(
                'mailResult' => $mailResult
            )
        );

        if ($mailResult == true):
            $args = array(
                'post_author' => 0,
                'post_content' => $full_name . ' ' . $email . ' ' . $phone_number . ' ' . $message,
                'post_title' => $subject,
                'post_status' => 'publish',
                'post_type' => 'flamingo_inbound'
            );
            $flamingo = wp_insert_post($args, true);
            $flamingo_metas = array(
                '_subject' => $subject,
                '_from' => $full_name . ' <' . $email . '>',
                '_from_name' => $full_name,
                '_from_email' => $email,
                '_field_fullname' => $full_name,
                '_field_email' => $email,
                '_field_phone' => $phone_number,
                '_field_message' => $message,
                '_fields' => array(
                    "fullname" => '',
                    "email" => '',
                    "phone" => '',
                    "message" => ''
                ),
                '_meta' => array(
                    "remote_ip" => $remote_ip,
                    "user_agent" => $user_agent,
                    "date" => $date,
                    "time" => $time,
                    "site_title" => 'Sawt al mustaqbal Application',
                    "site_description" => "Sawt al mustaqbal Application",
                    "site_url" => "http://www.sawtalmustaqbal.com",
                    "site_admin_email" => 'jean.elkhoury@trianglemena.com'
                )
            );

            foreach ($flamingo_metas as $key => $value):
                add_post_meta($flamingo, $key, $value);
            endforeach;

            $data = array(
                'status' => true,
                'message' => 'Flamingo Status',
                'data' => array(
                    'flamingo_post' => strip_tags($flamingo),
                    'mailResult' => $mailResult
                )
            );

//            //Send api response data
            echo json_response(200, $data);
        else:
            $data = array(
                'status' => true,
                'message' => 'Email Status',
                'data' => array(
                    'mailResult' => $mailResult
                )
            );

            //Send api response data
            echo json_response(200, $data);
        endif;

    else:
        //Send api response data
        $data = array(
            'status' => false,
            'error_code' => 'gc_004',
            'error_type' => 'general',
            'message' => 'An unknown error occurred',
        );

        echo json_response(200, $data);
        exit();
    endif;
}, "guest/email");
