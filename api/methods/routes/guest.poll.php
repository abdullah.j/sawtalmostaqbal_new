<?php

/**
 * @Route /api/guest/poll
 * @Type GET
 * @Variables:
 *
 * +returned_token: Valid generated JWT access token
 * +rating_number: Rating number
 *
 * @package Sawt al mustaqbal  
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer  Marc bou sleiman <http://marcbousleiman.com>
 *
 */
$router->map('POST', '/guest/poll', function () {

    extract($_POST);

    //GET sent variables
    $rating_number = isset($rating_number) ? $rating_number : '0';

    $error = 0;

    if (!$rating_number) {
        $error = 1;

        //Send api response data
        $data = array(
            'status' => false,
            'error_code' => 'gp_001',
            'error_type' => 'missing_vars',
            'message' => 'rating_number parameter missing',
        );


        echo json_response(200, $data);
        exit();
    }


    if (!$error) {
        global $wpdb;
        $table_name = $wpdb->prefix . "app_poll";
        $wpdb->insert($table_name, array(
            'rating_number' => $rating_number,
        ));
        if ($wpdb) {
            $data = array(
                'status' => true,
                'message' => 'Rating number saved',
                'data' => array(
                    'rating_number' => $rating_number
                )
            );
        } else {
            $data = array(
                'status' => false,
                'message' => 'Rating number not saved',
                'data' => array(
                    'rating_number' => $rating_number
                )
            );
        }
        echo json_response(200, $data);
        exit();
    } else {
        //Send api response data
        $data = array(
            'status' => false,
            'error_code' => 'gp_002',
            'error_type' => 'general',
            'message' => 'An unknown error occurred',
        );

        echo json_response(200, $data);
        exit();
    }
}, "guest/poll");
