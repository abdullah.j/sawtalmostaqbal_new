<?php

/**
 * @Route /api/guest/livestream
 * @Type GET
 * @Variables:
 *
 * +returned_token: Valid generated JWT access token
 *
 * @package Sawt al moustakbal  
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer Marc bou sleiman
 *
 */
$router->map('POST', '/guest/livestream', function () {

    $errors = 0;

    if (!$errors) {

        $livestream_link = get_field('live_streaming_link_api', 'options');

        $data = array(
            'status' => true,
            'message' => 'Livestream link',
            'data' => array(
                'livestream_url' => $livestream_link
            )
        );

        //Send api response data
        echo json_response(200, $data);
    } else {

        //Send api response data
        $data = array(
            'status' => false,
            'error_code' => 'gc_001',
            'error_type' => 'general',
            'message' => 'An unknown error occurred',
        );

        echo json_response(200, $data);
        exit();
    }
}, "guest/livestream");
