<?php

/**
 * @Route /api/guest/programs/single
 * @Type GET
 * @Variables:
 *
 * +returned_token: Valid generated JWT access token
 * +post_id: Post ID
 * +current_page: current pagination page
 *
 * @package Sawt al moustakbal  
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer Jean El Khoury
 *
 */
$router->map('POST', '/guest/programs/single', function () {

    //Timezone set
    date_default_timezone_set('Asia/Beirut');

    //GET sent vars
    extract($_POST);
    $post_id = isset($post_id) ? $post_id : "";
    $current_page = isset($current_page) ? $current_page : 1;
    $posts_per_page = isset($posts_per_page) ? $posts_per_page : 6;

    $errors = 0;

    if (!get_post_status($post_id)) {
        $errors = 1;

        //Send api response data
        $data = array(
            'status' => false,
            'error_code' => 'gues_001',
            'error_type' => 'missing_vars',
            'message' => 'invalid post_id',
        );

        echo json_response(200, $data);
        exit();
    }

    if (!$post_id) {

        $errors = 1;

        //Send api response data
        $data = array(
            'status' => false,
            'error_code' => 'gues_002',
            'error_type' => 'missing_vars',
            'message' => 'post_id parameter missing',
        );

        echo json_response(200, $data);
        exit();
    }

    if (!$errors) {
        //Vars init
        $data = [];
        $programs = [];
        $related_episodes = [];
        $count = 0;

        //this programs related episodes
        $offset = ($posts_per_page * $current_page) - $posts_per_page;
        $args_programs2 = array(
            'posts_per_page' => $posts_per_page,
            'paged' => $current_page,
            'offset' => $offset,
            'post_type' => 'programs_episodes',
            'order' => 'desc',
            'meta_key' => 'episode_program',
            'meta_value' => $post_id
        );
        $loop_programs2 = new WP_Query($args_programs2);

        if ($loop_programs2->have_posts()) {
            while ($loop_programs2->have_posts()) : $loop_programs2->the_post();

                $count++;
                $post_id2 = get_the_ID();
                $temp = sem_get_episodes($post_id2);
                $related_episodes[] = $temp;

            endwhile;
        }

        $maximum_episodes_pages = $loop_programs2->max_num_pages;

        if (($current_page + 1) > $maximum_episodes_pages) {
            $episodes_is_last_page = true;
        } else {
            $episodes_is_last_page = false;
        }

        //Query args
        $args = array(
            'post_type' => 'programs',
            'p' => $post_id
        );

        //Query
        $query = new WP_Query($args);
        if ($query->have_posts()):
            while ($query->have_posts()):
                $query->the_post();

                $count++;
                $post_id = get_the_ID();
                $temp = sem_get_programs($post_id);
                $programs[] = $temp;

            endwhile;
        endif;

        if (empty($programs)) {

            //Send api response data
            $data = array(
                'status' => true,
                'message' => 'No programs found',
            );
            echo json_response(200, $data);
        } else {

            //Send api response data
            $data = array(
                'status' => true,
                'message' => 'Single Programs',
                'data' => array(
                    'post_id' => $post_id,
                    'current_episodes_page' => $current_page,
                    'episodes_offset' => $offset,
                    'count' => $count,
                    'posts' => $programs,
                    'related_posts' => $related_episodes,
                    'episodes_last_page' => $episodes_is_last_page
                ),
            );

            echo json_response(200, $data);
        }
    } else {
        //Send api response data
        $data = array(
            'status' => false,
            'error_code' => 'gues_003',
            'error_type' => 'general',
            'message' => 'An unknown error occurred',
        );

        echo json_response(200, $data);
        exit();
    }
}, "guest/programs/single");
