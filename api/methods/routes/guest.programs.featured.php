<?php

/**
 * @Route /api/guest/programs/featured
 * @Type GET
 * @Variables:
 *
 * +returned_token: Valid generated JWT access token
 *
 * @package Sawt al moustakbal  
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer Jean El Khoury
 *
 */
$router->map('POST', '/guest/programs/featured', function () {

    //Timezone set
    date_default_timezone_set('Asia/Beirut');

    //GET sent vars
    extract($_POST);

    $errors = 0;

    //Vars init
    $data = [];
    $programs = [];
    $count = 0;

    //Query args   
    $args = array(
        'posts_per_page' => 1,
        'post_type' => 'programs',
        'order' => 'DESC',
        'orderby' => 'date',
        'meta_key' => 'featured_program',
        'meta_value' => 1
    );

    //Query
    $query = new WP_Query($args);
    if ($query->have_posts()):
        while ($query->have_posts()):
            $query->the_post();

            $count++;
            $post_id = get_the_ID();
            $temp = sem_get_programs($post_id);
            $programs[] = $temp;

        endwhile;
    endif;

    if (empty($programs)) {

        //Send api response data
        $data = array(
            'status' => true,
            'message' => 'No programs found',
        );
        echo json_response(200, $data);
    } else {

        //Send api response data
        $data = array(
            'status' => true,
            'message' => 'Programs',
            'data' => array(
                'count' => $count,
                'posts' => $programs,
            ),
        );

        echo json_response(200, $data);
    }
}, "guest/programs/featured");
