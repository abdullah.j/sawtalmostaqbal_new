<?php

/**
 * @Route /api/guest/episodes/homepage
 * @Type GET
 * @Variables:
 *
 * +returned_token: Valid generated JWT access token
 * +posts_per_page: posts per page
 * +current_page: current pagination page
 *
 * @package Sawt al moustakbal  
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer Jean El Khoury
 *
 */
$router->map('POST', '/guest/episodes/homepage', function () {

    //Timezone set
    date_default_timezone_set('Asia/Beirut');

    //GET sent vars
    extract($_POST);

    $posts_per_page = isset($posts_per_page) ? $posts_per_page : 5;
    $current_page = isset($current_page) ? $current_page : 1;

    $errors = 0;

    //Vars init
    $data = [];
    $episodes = [];
    $count = 0;

    //Query args
    $offset = ($posts_per_page * $current_page) - $posts_per_page;
    $args = array(
        'paged' => $current_page,
        'offset' => $offset,
        'posts_per_page' => $posts_per_page,
        'post_type' => 'programs_episodes',
        'order' => 'DESC',
        'meta_key' => 'episode_on_homepage',
        'meta_value' => 1
    );

    //Query
    $query = new WP_Query($args);
    if ($query->have_posts()):
        while ($query->have_posts()):
            $query->the_post();

            $count++;
            $post_id = get_the_ID();
            $temp = sem_get_episodes($post_id);
            $episodes[] = $temp;

        endwhile;
    endif;

    if (empty($episodes)) {

        //Send api response data
        $data = array(
            'status' => true,
            'message' => 'No episodes found',
        );
        echo json_response(200, $data);
    } else {

        //Send api response data
        $data = array(
            'status' => true,
            'message' => 'Episodes',
            'data' => array(
                'posts_per_page' => $posts_per_page,
                'current_page' => $current_page,
                'offset' => $offset,
                'count' => $count,
                'posts' => $episodes,
            ),
        );

        echo json_response(200, $data);
    }
}, "guest/episodes/homepage");
