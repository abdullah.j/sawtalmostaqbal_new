<?php
/**
 * Default single template page
 *
 * @package  TriTheme
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer  Marc bou sleiman <http://marcbousleiman.com>
 */
get_header('other');
if (have_posts()) {
    while (have_posts()) : the_post();
        ?>
        <div class="single_news_page padding_top">
            <div class="container-fluid">
                <div class="col-lg-12 col-md-12 col-sm-12 news_head">
                    <span onclick="goBack()" class="single_back_button">
                        <img alt="Exit logo" title="Exit" src="<?php echo get_template_directory_uri(); ?>/images/x-logo-black.png">
                    </span>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 top_right_description">
                    <?php
                    the_post_thumbnail('news_large', array('class' => 'img-responsive', 'alt' => get_the_title()));
                    ?>
                    <h1><?php echo get_the_title(); ?></h1>
                    <h4 class="date">
                        <?php
                        $postdate_d = get_the_date('D');
                        $postdate_d2 = get_the_date('d');
                        $postdate_m = get_the_date('M');
                        echo single_post_arabic_date($postdate_d2, $postdate_d, $postdate_m,'');
                        ?>
                    </h4>
                    <div class="single_news_desc">
                        <?php the_content(); ?>
                    </div>
        <!--                    <span class='button_item'>
                        <i class='fa fa-share-alt'></i>
                        <a class="addthis_button_compact"></a>
                    </span>-->
                    <div class="other_news_section" style="padding: 0;">
                        <h2 class="section_title" style="margin: 0;">شارك على</h2>
                    </div>
                    <ul class="social_media_list">
                        <li class="facebook"><a onclick="window.open('http://www.facebook.com/sharer/sharer.php?s=100&p[url]=<?php echo urlencode(get_permalink()); ?>&p[title]=<?php echo urlencode(get_the_title()); ?>', 'Facebook-dialog', 'width=626,height=436')" href="#"><i class="fa fa-facebook-f"></i></a></li>
                        <li class="twitter"><a onclick="window.open('http://twitter.com/intent/tweet/?text=<?php echo urlencode(get_the_title()); ?>&url=<?php echo urlencode(get_permalink()); ?>', 'Twitter-dialog', 'width=626,height=436')" href="#"><i class="fa fa-twitter"></i></a></li>
                        <li class="linked-in"><a onclick="window.open('https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode(get_permalink()); ?>&title=<?php echo urlencode(get_the_title()); ?>&source=LinkedIn')" href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li class="mailto"><a href="mailto:?subject=<?php echo get_the_title(); ?>&body=Check this article on <?php echo urlencode(get_permalink()); ?>"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></li>
                        <li class="whatsapp"><a href="https://api.whatsapp.com/send?phone=&text=Check this out <?php echo urlencode(get_permalink()); ?>"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                    </ul>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="other_news_section">
                <div class="container-fluid">
                    <?php
                    $args_news = array(
                        'post_type' => 'post',
                        'posts_per_page' => 4,
                        'order' => 'DESC',
                        'post__not_in' => array(get_the_ID())
                    );

                    $first_news = new WP_Query($args_news);

                    if ($first_news->have_posts()):
                        ?>
                        <h2 class="section_title">المزيد من الأخبار</h2>
                        <?php
                        $counter = 1;
                        while ($first_news->have_posts()):
                            $first_news->the_post();
                            ?>
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 small_div news_div">
                                <a href='<?php echo the_permalink(); ?>'><div class="inner_div">
                                        <div class="image_container">
                                            <?php echo the_post_thumbnail('news_small', array('class' => 'news_image_small img-responsive', 'alt' => 'news_grid')); ?>
                                            <p class="date" style="background-color : <?php echo $category_color[$taxonomy_term->name]; ?>">
                                                <?php
                                                $postdate_d = get_the_date('D');
                                                $postdate_d2 = get_the_date('d');
                                                $postdate_m = get_the_date('M');
                                                echo single_post_arabic_date($postdate_d2, $postdate_d, $postdate_m,'');
                                                ?>
                                            </p>
                                        </div>
                                        <div class="small_info">
                                            <p class="title">
                                                <?php custom_length(get_the_title(), 85); ?>
                                            </p>
                                        </div>
                                        <?php // the_ID(); ?>
                                    </div></a>
                            </div>
                            <?php
                            $counter++;
                        endwhile;
                        ?>

                        <?php
                    endif;
                    ?>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            function goBack() {
                window.history.back();
            }

            jQuery(window).load(function () {
                var height = jQuery(window).height();

                jQuery('.single_news_page').css('min-height', height - 44);

                jQuery('.top_header .middle_section ul li').removeClass('active');
                jQuery('.top_header .middle_section ul li#menu-item-117').addClass('active');
            });
        </script>
        <?php
    endwhile;
}
get_footer();
