<?php
/*
 * 
 * Single programs episodes
 * @package  TriTheme
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer  Marc bou sleiman <http://marcbousleiman.com>
 */
$this_id = get_the_ID();
if ($_GET['sid'] && ($this_id !== $_GET['sid'])) {
    $permalink = get_the_permalink($_GET['sid']);
    wp_redirect($permalink);
    die;
}
get_header();
while (have_posts()) : the_post();
    $parent_program = get_field('episode_program');
    $program_video_or_audio = get_field('program_audio_or_video', $parent_program);
    ?>
    <script type="text/javascript" src="http://w.soundcloud.com/player/api.js"></script>
    <div class="single_programs_episodes_page padding_top">
        <div class="container-fluid inner_episodes_wrapper">
            <img class="back_logo exit_btn" src="<?php echo get_template_directory_uri(); ?>/images/x-logo-white.png" title="Exit" alt="Exit">
            <h1 class="section_identifier"><?php echo get_the_title($parent_program); ?></h1>
            <div class="right_section col-lg-7 col-md-6 col-sm-6 col-xs-4">
                <?php
                $background_image = get_the_post_thumbnail_url($parent_program, 'full');
                ?>
                <?php if ($program_video_or_audio == 'audio') { ?>
                    <div class="inner_program_wrapper" style="background-image : url('<?php echo $background_image; ?>')">
                    </div>
                    <?php
                } else {
                    $video_id = get_field('episode_youtube_id');
                    $facebook_video_id = get_field('episode_facebook_source');
                    if ($video_id) {
                        ?>
                        <div class="inner_program_wrapper" style="background-image : url('<?php echo $background_image; ?>')">
                            <iframe scrolling="no" frameBorder="0"
                                    src="https://www.youtube.com/embed/<?php echo $video_id; ?>?rel=0">
                            </iframe> 
                        </div> 
                    <?php } else { ?>
                        <div class="image_holder">
                            <iframe scrolling="no" frameBorder="0"
                                    src="https://www.facebook.com/plugins/video.php?href=<?php echo $facebook_video_id; ?>&show_text=0">
                            </iframe>
                        </div>
                    <?php }
                    ?>
                <?php } ?>
            </div>
            <div class="left_section col-lg-5 col-md-6 col-sm-6 col-xs-8">
                <div class="programs_episodes_wrapper">
                    <div class="each_episode_wrapper active_episode" data-id="episode<?php echo $this_id; ?>">
                        <?php
                        $active_episode_iframe = get_field('soundcloud_iframe_code', $this_id);
                        echo $active_episode_iframe;
                        ?>
                        <div class="right_content">
                            <div class="image_holder">
                                <?php echo get_the_post_thumbnail($this_id, 'homepage_small_episode_img', array('class' => 'each_program_img', 'alt' => get_the_title($this_id), 'title' => get_the_title($this_id)));
                                ?>
                            </div>
                            <div class="title_holder">
                                <?php if ($program_video_or_audio == 'audio') { ?>
                                    <h3><?php custom_length(get_the_title($this_id), 40); ?></h3>
                                    <?php
                                } else {
                                    $video_id = get_field('episode_youtube_id');
                                    $facebook_video_id = get_field('episode_facebook_source');
                                    if ($video_id) {
                                        ?>
                                        <h3 class="init_video" data-src="<?php echo get_the_ID(); ?>" data-video_src="https://www.youtube.com/embed/<?php echo $video_id; ?>?rel=0"><?php custom_length(get_the_title($this_id), 60); ?></h3>  
                                    <?php } else { ?>
                                        <h3 class="init_video" data-src="<?php echo get_the_ID(); ?>" data-video_src="https://www.facebook.com/plugins/video.php?href=<?php echo $facebook_video_id; ?>&show_text=0"><?php custom_length(get_the_title($this_id), 60); ?></h3>  
                                    <?php }
                                    ?>
                                <?php } ?>
                            </div>
                        </div>
                        <?php if ($active_episode_iframe) { ?>
                            <div class="left_content">
                                <span class="arrow_init" data-src="<?php echo $this_id; ?>">
                                    <i class = "fa fa-play" aria-hidden = "true"></i>
                                </span>
                                <div id="bars">
                                    <div class="bar"></div>
                                    <div class="bar"></div>
                                    <div class="bar"></div>
                                </div>

                            </div>
                        <?php } ?>
                        <div class="clear"></div>
                    </div>
                    <?php
                    $args_episodes = array(
                        'post_type' => 'programs_episodes',
                        'posts_per_page' => 40,
                        'order' => 'desc',
                        'post_status' => 'publish',
                        'meta_key' => 'episode_program',
                        'meta_value' => $parent_program,
                        'post__not_in' => array($this_id)
                    );

                    $first_episodes = new WP_Query($args_episodes);

                    if ($first_episodes->have_posts()):
                        while ($first_episodes->have_posts()):
                            $first_episodes->the_post();
                            $parent_program = get_field('episode_program');
                            $program_day = get_field('program_day', $parent_program);

                            $program_day_readable = '';

                            switch ($program_day) {
                                case "1":
                                    $program_day_readable = "الاثنين";
                                    break;

                                case "2":
                                    $program_day_readable = "الثلاثاء";
                                    break;

                                case "3":
                                    $program_day_readable = "الاربعاء";
                                    break;

                                case "4":
                                    $program_day_readable = "الخميس";
                                    break;
                                case "5":
                                    $program_day_readable = "الجمعة";
                                    break;

                                case "6":
                                    $program_day_readable = "السبت";
                                    break;

                                case "7":
                                    $program_day_readable = "الأحد";
                                    break;

                                default:
                                    echo "";
                            }

                            $parent_program_time_from = get_field('program_time_from', $parent_program);
                            $parent_program_time_to = get_field('program_time_to', $parent_program);
                            ?>
                            <div class="each_episode_wrapper" data-id="episode<?php echo get_the_id(); ?>">
                                <?php
                                $active_episode_iframe = get_field('soundcloud_iframe_code');
                                echo $active_episode_iframe;
                                ?>
                                <div class="right_content">
                                    <div class="image_holder">
                                        <?php echo the_post_thumbnail('homepage_small_episode_img', array('class' => 'each_program_img', 'alt' => get_the_title(), 'title' => get_the_title()));
                                        ?>
                                    </div>
                                    <div class="title_holder">
                                        <?php if ($program_video_or_audio == 'audio') { ?>
                                            <h3><?php custom_length(get_the_title(), 40); ?></h3>
                                            <?php
                                        } else {
                                            $video_id = get_field('episode_youtube_id');
                                            $facebook_video_id = get_field('episode_facebook_source');
                                            if ($video_id) {
                                                ?>
                                                <h3 class="init_video" data-src="<?php echo get_the_ID(); ?>" data-video_src="https://www.youtube.com/embed/<?php echo $video_id; ?>?rel=0"><?php custom_length(get_the_title(), 60); ?></h3>  
                                            <?php } else { ?>
                                                <h3 class="init_video" data-src="<?php echo get_the_ID(); ?>" data-video_src="https://www.facebook.com/plugins/video.php?href=<?php echo $facebook_video_id; ?>&show_text=0"><?php custom_length(get_the_title(), 60); ?></h3>  
                                            <?php }
                                            ?>
                                        <?php } ?>
                                    </div>
                                </div>
                                <?php if ($active_episode_iframe) { ?>
                                    <div class="left_content">
                                        <span class="arrow_init" data-src="<?php echo get_the_ID(); ?>">
                                            <i class = "fa fa-play" aria-hidden = "true"></i>
                                        </span>
                                        <div id="bars">
                                            <div class="bar"></div>
                                            <div class="bar"></div>
                                            <div class="bar"></div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="clear"></div>
                            </div>
                            <?php
                        endwhile;
                    endif;
                    ?>
                </div>
            </div>
        </div>
        <div class="blacked_overlay"></div>
        <div class="blurred_overlay" style="background-image: url(<?php echo get_the_post_thumbnail_url($parent_program, 'full'); ?>)"></div>
    </div>
    <script type="text/javascript">
        jQuery(window).load(function () {
            var height = jQuery(window).height();
            var top_header_height = jQuery('.top_header').height();
            var bottom_player_height = jQuery('.footer').height() + 30;

            var top_plus_bottom = top_header_height + bottom_player_height;

            var width = jQuery(window).width();

            if (width > 500) {
                jQuery('.single_programs_episodes_page').height(height - top_plus_bottom);
            } else {
                jQuery('.single_programs_episodes_page').css('min-height', height - top_plus_bottom);
            }

            //featured programs sections height
            var left_Section_h = jQuery('.single_programs_episodes_page .left_section').height();
            jQuery('.single_programs_episodes_page .right_section .inner_program_wrapper').height(left_Section_h);

            jQuery(".active_episode .arrow_init").click();
        });

        jQuery(document).ready(function ($) {
            var width = jQuery(window).width();
            if (width > 768) {
                jQuery(".single_programs_episodes_page .left_section .programs_episodes_wrapper").niceScroll({
                    cursorcolor: "#63B466",
                    background: "rgba(255,255,255,0.2)",
                    autohidemode: false,
                    cursorborder: "1px solid #63B466",
                    railalign: 'left'
                });
            }

            jQuery('.programs_episodes_wrapper .each_episode_wrapper').each(function () {
                var parent_id = jQuery(this).attr('data-id');
                jQuery(this).find('iframe').attr('id', parent_id);
            });

            //            var widget1 = SC.Widget('episode33');

            jQuery(function () {
                jQuery(document).on('click', ".arrow_init", function () {

                    //                    jQuery('#sp-audio').get(0).pause();

                    window.history.pushState('page2', 'Title', '?sid=' + jQuery(this).attr('data-src'));

                    jQuery('.arrow_init').closest('.each_episode_wrapper').removeClass('active_episode');
                    jQuery(this).closest('.each_episode_wrapper').addClass('active_episode');

                    jQuery('.arrow_init').find('i').removeClass('fa-pause').addClass('fa-play');

                    var parent_id = jQuery(this).closest('.each_episode_wrapper').find('iframe').attr('id');
                    var widget1 = SC.Widget(parent_id);

                    if (jQuery(this).hasClass('played')) {
                        jQuery(this).removeClass('played');
                        jQuery(this).find('i').removeClass('fa-pause').addClass('fa-play');
                        widget1.pause();
                    } else {
                        jQuery('.arrow_init').removeClass('played');
                        jQuery(this).addClass('played');
                        jQuery(this).find('i').removeClass('fa-play').addClass('fa-pause');
                        widget1.play();
                    }
                });
            });

            //on click of video title
            jQuery('.single_programs_episodes_page .left_section .programs_episodes_wrapper .each_episode_wrapper .right_content .title_holder h3.init_video').on('click', function () {
                var video_id = jQuery(this).attr('data-video_src');
                window.history.pushState('page2', 'Title', '?sid=' + jQuery(this).attr('data-src'));
                jQuery('.single_programs_episodes_page .left_section .programs_episodes_wrapper .each_episode_wrapper').removeClass('active_episode');
                jQuery(this).closest('.each_episode_wrapper').addClass('active_episode');
                jQuery('.single_programs_episodes_page .inner_episodes_wrapper .right_section .inner_program_wrapper iframe').attr('src', video_id);
            });

        });
    </script>
    <?php
endwhile;
get_footer();
