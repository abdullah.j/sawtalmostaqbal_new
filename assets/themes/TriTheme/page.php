<?php
/**
 * Default page template
 *
 * @package  TriTheme
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer  Marc bou sleiman <http://marcbousleiman.com>
 */
get_header();
if (have_posts()):
    while (have_posts()):
        the_post();
        ?>
        <h2><?php the_title(); ?></h2>
        <p><?php the_content(); ?></p>
        <?php
        the_post_thumbnail('full', array('class' => 'img-responsive', 'title' => 'yo', 'alt' => 'yo'));
    endwhile;
endif;
get_footer();
