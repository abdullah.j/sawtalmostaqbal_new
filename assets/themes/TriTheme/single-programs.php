<?php
/**
 * Single programs page
 *
 * @package  TriTheme
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer  Marc bou sleiman <http://marcbousleiman.com>
 */
get_header();

$referer = $_SERVER["HTTP_REFERER"];
$programs_page_url = esc_url(home_url('/programs/'));
$program_logo_url = get_field('program_logo', get_the_ID());
while (have_posts()) : the_post();
    $this_id = get_the_ID();
    ?>
    <script type="text/javascript" src="http://w.soundcloud.com/player/api.js"></script>
    <div class="single_programs_page padding_top">
        <div class="top_identifier_section">
            <?php if ($programs_page_url == $referer) { ?>
                <img class="back_logo exit_btn" src="<?php echo get_template_directory_uri(); ?>/images/x-logo-white.png" title="Exit" alt="Exit">
            <?php } ?>
            <?php
            $args_programs = array(
                'posts_per_page' => 1,
                'post_type' => 'programs_episodes',
                'order' => 'DESC',
                'meta_key' => 'episode_program',
                'meta_value' => $this_id
            );
            $loop_programs = new WP_Query($args_programs);

            if ($loop_programs->have_posts()) {
                while ($loop_programs->have_posts()) : $loop_programs->the_post();
                    $first_episode = get_the_ID();
                endwhile;
                wp_reset_query();
            }
            $video_or_audio = get_field('video_or_audio', $first_episode);
            if ($video_or_audio == 'video') {
                $video_id = get_field('episode_youtube_id', $first_episode);
                $facebook_video_id = get_field('episode_facebook_source', $first_episode);
                if ($video_id) {
                    if($program_logo_url){ ?>
                    <div class="programLogo">
                        <img src="<?php echo $program_logo_url ?>" alt="Logo">
                    </div>
                    <?php } ?>
                    <div class="image_holder">
                        <iframe scrolling="no" frameBorder="0"
                                src="https://www.youtube.com/embed/<?php echo $video_id; ?>?rel=0">
                        </iframe>
                    </div>
                    <?php
                }
                if ($facebook_video_id) {
                    ?>
                    <div class="image_holder">
                        <iframe scrolling="no" frameBorder="0"
                                src="https://www.facebook.com/plugins/video.php?href=<?php echo $facebook_video_id; ?>&show_text=0">
                        </iframe>
                    </div>
                <?php }
                ?>
            <?php } else {
                ?>
                <div class="image_holder audio_holder" data-id="episode<?php echo $first_episode; ?>">
                    <?php
                    $active_episode_iframe = get_field('soundcloud_iframe_code', $first_episode);
                    echo get_the_post_thumbnail($this_id, 'single_program_top_img', array('class' => 'single_program_img', 'alt' => get_the_title(), 'title' => get_the_title()));
                    echo $active_episode_iframe;
                    if ($active_episode_iframe) {
                        ?>
                        <img class="play_button arrow_init" data-pause_button="<?php echo get_template_directory_uri(); ?>/images/pause-button.png" data-play_button="<?php echo get_template_directory_uri(); ?>/images/play-button.png" src="<?php echo get_template_directory_uri(); ?>/images/play-button.png" title="Play" alt="Play">
                    <?php } ?>
                </div>
            <?php }
            ?>
            <div class="description_holder">
                <h1><?php echo get_the_title(); ?></h1>
                <div class="content">
                    <p><?php echo get_the_content(); ?></p>
                </div>
                <span class='button_item'>
                    <img class="sharing_icon" src="<?php echo get_template_directory_uri(); ?>/images/share-icon.png" title="Share" alt="Share">
                    <a title="Share" class="addthis_button_compact"></a>
                </span>
            </div>
        </div>
        <?php
        $args_programs2 = array(
            'posts_per_page' => -1,
            'post_type' => 'programs_episodes',
            'order' => 'desc',
            'meta_key' => 'episode_program',
            'meta_value' => $this_id
        );
        $loop_programs2 = new WP_Query($args_programs2);

        if ($loop_programs2->have_posts()) {
            ?>
            <div class="episodes_wrapper">
                <h2 class="section_identifier">مقاطع</h2>
                <div dir="rtl" class="swiper-container s2">
                    <div class="swiper-wrapper">
                        <?php
                        while ($loop_programs2->have_posts()) : $loop_programs2->the_post();
                            ?>
                            <div class="swiper-slide">
                                <a href="<?php echo get_the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                    <div class="program_swipe_wrapper">
                                        <div class="swipe_image_holder">
                                            <img src="https://img.youtube.com/vi/<?php echo get_field('episode_youtube_id', get_the_ID()); ?>/hqdefault.jpg" alt="Thumbnail">
                                        </div>
                                        <h3><?php custom_length(get_the_title(), 60); ?></h3>
                                    </div>
                                </a>
                            </div>
                            <?php
                        endwhile;
                        ?>
                    </div>
                    <!--Add Arrows -->
                    <div class = "swiper-button-next s_episodes_section_nav">
                        <i class = "fa fa-angle-right" aria-hidden = "true"></i>
                    </div>
                    <div class = "swiper-button-prev s_episodes_section_nav">
                        <i class = "fa fa-angle-left" aria-hidden = "true"></i>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
    <?php if ($this_id == 26) { ?>
        <script type="text/javascript">
            jQuery('.top_header .middle_section ul li,.top_mobile_menu_section ul li').removeClass('active');
            jQuery('.top_header .middle_section ul li.menu-item-92,.top_mobile_menu_section ul li.menu-item-92').addClass('active');
        </script>
    <?php }
    if ($this_id == 1236) {
        ?>
        <script type="text/javascript">
            jQuery('.top_header .middle_section ul li,.top_mobile_menu_section ul li').removeClass('active');
            jQuery('.top_header .middle_section ul li.menu-item-1301,.top_mobile_menu_section ul li.menu-item-1301').addClass('active');
        </script>
    <?php }
    //online
//    if ($this_id == 11186) {
//        ?>
<!--        <script type="text/javascript">-->
<!--            jQuery('.top_header .middle_section ul li,.top_mobile_menu_section ul li').removeClass('active');-->
<!--            jQuery('.top_header .middle_section ul li.menu-item-11680,.top_mobile_menu_section ul li.menu-item-11680').addClass('active');-->
<!--        </script>-->
<!--    --><?php //}
//    if ($this_id == 11181) {
//        ?>
<!--        <script type="text/javascript">-->
<!--            jQuery('.top_header .middle_section ul li,.top_mobile_menu_section ul li').removeClass('active');-->
<!--            jQuery('.top_header .middle_section ul li.menu-item-11679,.top_mobile_menu_section ul li.menu-item-11679').addClass('active');-->
<!--        </script>-->
<!--    --><?php //}
    if ($this_id == 11186) {
        ?>
        <script type="text/javascript">
            jQuery('.top_header .middle_section ul li,.top_mobile_menu_section ul li').removeClass('active');
            jQuery('.top_header .middle_section ul li.menu-item-11509,.top_mobile_menu_section ul li.menu-item-11509').addClass('active');
        </script>
    <?php }
    if ($this_id == 11181) {
        ?>
        <script type="text/javascript">
            jQuery('.top_header .middle_section ul li,.top_mobile_menu_section ul li').removeClass('active');
            jQuery('.top_header .middle_section ul li.menu-item-11508,.top_mobile_menu_section ul li.menu-item-11508').addClass('active');
        </script>
    <?php }
    ?>
    <script type="text/javascript">
        var width = jQuery(window).width();
        var slides_per_view = 3;
        if (width < 992) {
            slides_per_view = 2.3;
        }
        if (width < 768) {
            slides_per_view = 1.8;
        }
        if (width < 500) {
            slides_per_view = 1.4;
        }
        if (width < 400) {
            slides_per_view = 1.2;
        }
        //swiper second
        var swiper2 = new Swiper('.episodes_wrapper .swiper-container.s2', {
            navigation: {
                nextEl: '.episodes_wrapper .s_episodes_section_nav.swiper-button-prev',
                prevEl: '.episodes_wrapper .s_episodes_section_nav.swiper-button-next'
            },
            slidesPerView: slides_per_view,
            centeredSlides: false,
            spaceBetween: 30,
        });

        var parent_id = jQuery('.single_programs_page .top_identifier_section .image_holder').attr('data-id');
        jQuery('.single_programs_page .top_identifier_section .image_holder').find('iframe').attr('id', parent_id);

        jQuery(function () {
            jQuery(document).on('click', ".arrow_init", function () {

                var play_button = jQuery(this).attr('data-play_button');
                var pause_button = jQuery(this).attr('data-pause_button');

                var parent_id = jQuery(this).closest('.image_holder').find('iframe').attr('id');
                var widget1 = SC.Widget(parent_id);

                if (jQuery(this).hasClass('played')) {
                    jQuery(this).removeClass('played');
                    jQuery(this).attr('src', play_button);
                    widget1.pause();
                } else {
                    jQuery('.arrow_init').removeClass('played');
                    jQuery(this).addClass('played');
                    jQuery(this).attr('src', pause_button);
                    widget1.play();
                }
            });
        });
    </script>
    <?php
endwhile;
get_footer();
