<?php
/**
 * Default tag archive
 *
 * @package  TriTheme
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer  Marc bou sleiman <http://marcbousleiman.com>
 */
get_header();
$queried_object = get_queried_object();
$term_id = $queried_object->term_id;
$term_name = $queried_object->name;
?>
<?php
get_footer();

