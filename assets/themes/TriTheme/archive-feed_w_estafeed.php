<?php
/**
 * Feed w estafeed page
 *
 * @package  TriTheme
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer  Marc bou sleiman <http://marcbousleiman.com>
 */
get_header();

$nonce = wp_create_nonce("filter_feed_posts");
$link = admin_url('admin-ajax.php');

$nonce_infinite = wp_create_nonce("infinite_feed_posts");
$link_infinite = admin_url('admin-ajax.php');
?>
<div class="feed_w_estafeed_page padding_top">
    <div class="title_and_filter">
        <h1 class="page_title">فيد واستفيد</h1>
        <div class="filters_holder" data-nonce="<?php echo $nonce; ?>" data-link="<?php echo $link; ?>">
            <div id="location_selector" class="selector_wrapper">
                <div class="selection_garage dont_close">
                    <h3 class="dont_close">الأماكن</h3>
                    <span class="dont_close"><i class="fa fa-angle-down dont_close"></i></span>
                    <div class="clear"></div>
                </div>
                <?php
                $locations_terms = get_terms(array(
                    'taxonomy' => 'feedwestafeed-locations',
                    'hide_empty' => true
                ));
                ?>
                <ul class="filtering_dropdown makeMeScrollDropdown">
                    <li class="clearFilter" data-filter_id=""
                        data-html="الأماكن">مسح النتائج</li>
                        <?php
                        if ($locations_terms) {
                            foreach ($locations_terms as $locations_term) {
                                ?>
                            <li class="dont_close" data-filter_id="<?php echo $locations_term->term_id; ?>"
                                data-html="<?php echo $locations_term->name; ?>"><?php echo $locations_term->name; ?></li>
                                <?php
                            }
                        }
                        ?>
                </ul>
            </div>
            <div id="category_selector" class="selector_wrapper">
                <div class="selection_garage dont_close">
                    <h3 class="dont_close">النشاطات</h3>
                    <span class="dont_close"><i class="fa fa-angle-down dont_close"></i></span>
                    <div class="clear"></div>
                </div>
                <?php
                $categories_terms = get_terms(array(
                    'taxonomy' => 'feedwestafeed-categories',
                    'hide_empty' => true
                ));
                ?>
                <ul class="filtering_dropdown makeMeScrollDropdown">
                    <li class="clearFilter" data-filter_id=""
                        data-html="النشاطات">مسح النتائج</li>
                        <?php
                        if ($categories_terms) {
                            foreach ($categories_terms as $categories_term) {
                                ?>
                            <li class="dont_close" data-filter_id="<?php echo $categories_term->term_id; ?>"
                                data-html="<?php echo $categories_term->name; ?>"><?php echo $categories_term->name; ?></li>
                                <?php
                            }
                        }
                        ?>
                </ul>
            </div>
            <span class="loader"><i class="fa fa-spinner" aria-hidden="true"></i></span>
            <div class="ajax_response"></div>
        </div>
    </div>

    <div class="posts_holder">
        <div class="container-fluid">
            <?php
            $args = array(
                'post_type' => 'feed_w_estafeed',
                'posts_per_page' => 12,
                'order' => 'desc',
                'post_status' => 'publish'
            );

            $first = new WP_Query($args);

            if ($first->have_posts()):
                while ($first->have_posts()):
                    $first->the_post();
                    $feedwestafeed_phone_number = get_field('feedwestafeed_phone_number');
                    $feedwestafeed_website = get_field('feedwestafeed_website');
                    $category = get_the_terms(get_the_id(), 'feedwestafeed-categories');
                    $locations = get_the_terms(get_the_id(), 'feedwestafeed-locations');
                    $main_article_image = get_the_post_thumbnail_url(get_the_id(), 'full');
                    $feedwestafeed_youtube_id = get_field('feedwestafeed_youtube_id');
                    ?>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 each_post_wrap">
                        <div class="inner_each_post_wrap">
                            <div class="mage_holder">
                                <img src="<?php echo $main_article_image; ?>"
                                     title="<?php echo get_the_title(); ?>"
                                     alt="<?php echo get_the_title(); ?> Image"/>
                            </div>
                            <div class="description">
                                <ul>
                                    <li>
                                        <span class="cust_label">وصف:</span>
                                        <span class="cust_value"><?php echo get_the_content(); ?></span>
                                    </li>
                                </ul>
                                <div class="right_side">
                                    <ul>
                                        <li>
                                            <span class="cust_label">الاسم:</span>
                                            <span class="cust_value"><?php echo get_the_title(); ?></span>
                                        </li>
                                        <?php if ($category[0]->name) { ?>
                                            <li>
                                                <span class="cust_label">النشاط:</span>
                                                <span class="cust_value"><?php echo $category[0]->name; ?></span>
                                            </li>
                                        <?php } ?>
                                        <?php if ($locations[0]->name) { ?>
                                            <li>
                                                <span class="cust_label">المكان:</span>
                                                <span class="cust_value"><?php echo $locations[0]->name; ?></span>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <div class="left_side">
                                    <ul>
                                        <?php if ($feedwestafeed_phone_number) { ?>
                                            <li>
                                                <span class="cust_label">الهاتف:</span>
                                                <span class="cust_value"><a href="tel:<?php echo $feedwestafeed_phone_number; ?>"><?php echo $feedwestafeed_phone_number; ?>+</a></span>
                                            </li>
                                        <?php } ?>
                                        <?php if ($feedwestafeed_website) { ?>
                                            <li>
                                                <span class="cust_label">الموقع:</span>
                                                <span class="cust_value"><a target="_blank" href="<?php echo $feedwestafeed_website; ?>">زيارة الموقع</a></span>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <ul>
                                    <?php if ($feedwestafeed_youtube_id) { ?>
                                    <li>
                                        <span class="cust_label">فيديو:</span>
                                        <span class="cust_value"><a target="_blank" href="https://www.youtube.com/embed/<?php echo $feedwestafeed_youtube_id; ?>?rel=0">الرابط</a></span>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php
                endwhile;
            endif;

            $current_page = 1;
            $maximum_number_of_news = $first->max_num_pages;

            if (( $current_page + 1 ) > $maximum_number_of_news) {
                $news_is_last_page = true;
            } else {
                ?>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 get_more_btn">
                    <a class="next-page custom_scroll" href="#"
                       data-count="2" data-link="<?php echo $link_infinite; ?>"
                       data-nonce="<?php echo $nonce_infinite; ?>">
                        <span>المزيد</span>
                    </a>
                    <span class="loader">جاري التحميل...</span>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>
<script type="text/javascript">
//trigger open category list
    jQuery('.selector_wrapper .selection_garage').on('click', function () {
        if (jQuery(this).parent().hasClass('menu_opened')) {
            jQuery(this).parent().removeClass('menu_opened');
            jQuery(this).parent().find('ul.filtering_dropdown').removeClass('opened');
            jQuery(this).parent().find('ul.filtering_dropdown').fadeOut();
        } else {
            jQuery('.selector_wrapper').removeClass('menu_opened');
            jQuery('.selector_wrapper').find('ul.filtering_dropdown').removeClass('opened');
            jQuery('.selector_wrapper').find('ul.filtering_dropdown').fadeOut();

            jQuery(this).parent().find('ul.filtering_dropdown').fadeIn();
            jQuery(this).parent().addClass('menu_opened');
            jQuery(this).parent().find('ul.filtering_dropdown').addClass('opened');
        }
    });

    jQuery(document).on('click', '.selector_wrapper ul.filtering_dropdown li', function () {
        var element = jQuery(this);

        element.closest('.selector_wrapper').find('ul.filtering_dropdown li').removeClass('selected');
        element.addClass('selected');

        var elmntHtml = element.attr('data-html');

        element.closest('.selector_wrapper').find('.selection_garage h3').html(elmntHtml);

        var ajax_url = jQuery('.filters_holder').attr('data-link');
        var nonce = jQuery('.filters_holder').attr('data-nonce');

        var loader = jQuery('.filters_holder .loader');

        var locationId = jQuery('#location_selector ul li.selected').attr('data-filter_id');
        var categoryId = jQuery('#category_selector ul li.selected').attr('data-filter_id');

        loader.fadeIn('slow');
        jQuery('.posts_holder').css('opacity', '0.4');

        jQuery.ajax({
            type: "POST",
            dataType: "json",
            url: ajax_url,
            data: {action: "filter_feed_posts", nonce: nonce, location_id: locationId, category_id: categoryId},
            success: function (response) {
                jQuery('.posts_holder').css('opacity', '1');
                if (response.type == "success") {
                    loader.fadeOut('slow');
                    jQuery('.posts_holder .container-fluid').html(response.html);
                } else {
                    jQuery('.ajax_response').html('<h5 style="color : red ">There was an error! Please try again.</h5>');
                }
            }
        });

    });

    jQuery(document).on('click', '.custom_scroll', function (e) {
        e.preventDefault();

        var element = jQuery(this);

        var loader = element.closest('.get_more_btn').find('.loader');
        var nonce = element.attr("data-nonce");
        var ajax_url = element.attr("data-link");
        var count = element.attr('data-count');

        var locationId = jQuery('#location_selector ul li.selected').attr('data-filter_id');
        var categoryId = jQuery('#category_selector ul li.selected').attr('data-filter_id');

        jQuery(this).remove();
        loader.fadeIn().css('display', 'inline-block');

        jQuery.ajax({
            type: "POST",
            dataType: "json",
            url: ajax_url,
            data: {
                action: "infinite_feed_posts",
                nonce: nonce,
                current_page: count,
                location_id: locationId,
                category_id: categoryId
            },
            success: function (response) {
                if (response.type == "success") {
                    loader.parent().remove();
                    jQuery('.posts_holder .container-fluid').append(response.html);
                } else {
                    loader.html('<h5 style="color : red ">There was an error! Please try again.</h5>');
                }
            }
        });
    });
</script>
<?php
get_footer();
