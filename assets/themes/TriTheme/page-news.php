<?php
/**
 * Template name: News
 *
 * @package  TriTheme
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer  Marc bou sleiman <http://marcbousleiman.com>
 */
get_header('other');
?>
<div class="archive_news_page padding_top">
    <div class="container-fluid">
        <?php
        $paged1 = isset($_GET['paged1']) ? (int) $_GET['paged1'] : 1;
        $args_news = array(
            'post_type' => 'post',
            'posts_per_page' => 8,
            'order' => 'DESC',
            'paged' => $paged1
        );

        $first_news = new WP_Query($args_news);

        if ($first_news->have_posts()):
            ?>
            <h2 class="section_title">أبرز الأخبار</h2>
            <div class="news_to_infinite_scroll">
                <?php
                $counter = 1;
                while ($first_news->have_posts()):
                    $first_news->the_post();
                    ?>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 small_div news_div">
                        <a href='<?php echo the_permalink(); ?>'><div class="inner_div">
                                <div class="image_container">
                                    <?php echo the_post_thumbnail('news_small', array('class' => 'news_image_small img-responsive', 'alt' => 'news_grid')); ?>
                                    <p class="date" style="background-color : <?php echo $category_color[$taxonomy_term->name]; ?>">
                                        <?php
                                        $postdate_d = get_the_date('D');
                                        $postdate_d2 = get_the_date('d');
                                        $postdate_m = get_the_date('M');
                                        echo single_post_arabic_date($postdate_d2, $postdate_d, $postdate_m);
                                        ?>
                                    </p>
                                </div>
                                <div class="small_info">                                    
                                    <p class="title">
                                        <?php custom_length(get_the_title(), 85); ?>
                                    </p>
                                </div>
                                <?php // the_ID(); ?>
                            </div></a>
                    </div>
                    <?php
                    $counter++;
                endwhile;
                ?>

                <?php
            endif;
            ?>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 get_more_btn">
                <?php
                $pag_args = array(
                    'format' => '?paged1=%#%',
                    'current' => $paged1,
                    'total' => $first_news->max_num_pages,
                    'next_text' => '<span>المزيد</span>',
                );
                echo paginate_links($pag_args);
                ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery('.archive_news_page .news_to_infinite_scroll').jscroll({
        loadingHtml: '<span class="loader"></span>',
        padding: 20,
        nextSelector: '.get_more_btn a.next',
        contentSelector: '.archive_news_page .news_to_infinite_scroll',
        autoTrigger: false
    });

    jQuery(window).load(function () {
        var height = jQuery(window).height();
        
        jQuery('.archive_news_page').css('min-height',height - 44);
    });
</script>
<?php
get_footer();
