<?php
/*
 * 
 * Template name: About us
 * 
 */
get_header();
?>
<div id="about_us_page" class="about_us_page padding_top">
    <?php
    $about_us_title = get_field('title', 'option');
    $about_us_content = get_field('content', 'option');
    $about_us_youtube_id = get_field('youtube_id', 'option');
    $about_us_contactus_link_text = get_field('contactus_link_text', 'option');
    ?>
    <div class="content">
        <?php if($about_us_youtube_id): ?>
        <div class="image_holder">
            <iframe scrolling="no" frameBorder="0"
                    src="https://www.youtube.com/embed/<?php echo $about_us_youtube_id; ?>?rel=0">
            </iframe>
        </div>
        <?php endif; ?>
        <h1><?php echo $about_us_title; ?></h1>
        <p><?php echo $about_us_content; ?></p>
        <h2><a href="<?php echo get_permalink( get_page_by_path( 'contact-us' ) ) ?>"><?php echo $about_us_contactus_link_text ?></a></h2>
    </div>
</div>
<script type = "text/javascript">
    jQuery(window).load(function () {
        var height = jQuery(window).height();
        var top_header_height = jQuery('.top_header').height();
        var bottom_player_height = jQuery('.footer').height() + 30;
        
        var top_plus_bottom = top_header_height + bottom_player_height;
        
        var width = jQuery(window).width();
        
        if (width > 768) {
            // jQuery('.about_us_page').height(height - top_plus_bottom);
        }else{
            // jQuery('.about_us_page').css( 'min-height' , height - top_plus_bottom);
        }
    });
</script>
<?php
get_footer();
