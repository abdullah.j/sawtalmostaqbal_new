<?php

/**
 * Sample ajax
 *
 * @package  TriTheme
 * @developer  Maroun Melhem
 * @developer  _REPLACE_WITH_DEV_NAME_
 *
 */
add_action('wp_ajax_infinite_feed_posts', 'prefix_ajax_infinite_feed_posts');
add_action('wp_ajax_nopriv_infinite_feed_posts', 'prefix_ajax_infinite_feed_posts');

function prefix_ajax_infinite_feed_posts() {
    if (!wp_verify_nonce($_REQUEST['nonce'], "infinite_feed_posts")) {
        exit("You think you are smart?");
    }

    $nonce_infinite = wp_create_nonce("infinite_feed_posts");
    $link_infinite = admin_url('admin-ajax.php');

    $posts_per_page_stripped = 12;
    $current_page_stripped = strip_tags($_REQUEST['current_page']);

    $posts_per_page = isset($posts_per_page_stripped) ? $posts_per_page_stripped : 12;
    $current_page = isset($current_page_stripped) ? $current_page_stripped : 1;
    $offset = ( $posts_per_page * $current_page ) - $posts_per_page;

    $location_id = strip_tags($_REQUEST['location_id']);
    $category_id = strip_tags($_REQUEST['category_id']);

    $result['type'] = "success";
    $result['html'] = "";

    if ($location_id && !$category_id) {
        $args = array(
            'post_type' => 'feed_w_estafeed',
            'paged' => $current_page,
            'offset' => $offset,
            'posts_per_page' => $posts_per_page,
            'order' => 'desc',
            'post_status' => 'publish',
            'tax_query' => array(
                array(
                    'taxonomy' => 'feedwestafeed-locations',
                    'field' => 'id',
                    'terms' => $location_id,
                )
            )
        );
    }

    if (!$location_id && $category_id) {
        $args = array(
            'post_type' => 'feed_w_estafeed',
            'paged' => $current_page,
            'offset' => $offset,
            'posts_per_page' => $posts_per_page,
            'order' => 'desc',
            'post_status' => 'publish',
            'tax_query' => array(
                array(
                    'taxonomy' => 'feedwestafeed-categories',
                    'field' => 'id',
                    'terms' => $category_id
                )
            )
        );
    }

    if ($location_id && $category_id) {
        $args = array(
            'post_type' => 'feed_w_estafeed',
            'paged' => $current_page,
            'offset' => $offset,
            'posts_per_page' => $posts_per_page,
            'order' => 'desc',
            'post_status' => 'publish',
            'tax_query' => array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'feedwestafeed-locations',
                    'field' => 'id',
                    'terms' => $location_id,
                ),
                array(
                    'taxonomy' => 'feedwestafeed-categories',
                    'field' => 'id',
                    'terms' => $category_id
                )
            )
        );
    }

    if (!$location_id && !$category_id) {
        $args = array(
            'post_type' => 'feed_w_estafeed',
            'paged' => $current_page,
            'offset' => $offset,
            'posts_per_page' => $posts_per_page,
            'order' => 'desc',
            'post_status' => 'publish'
        );
    }

    $first = new WP_Query($args);

    if ($first->have_posts()):
        while ($first->have_posts()):
            $first->the_post();
            $feedwestafeed_phone_number = get_field('feedwestafeed_phone_number');
            $feedwestafeed_website = get_field('feedwestafeed_website');
            $category = get_the_terms(get_the_id(), 'feedwestafeed-categories');
            $locations = get_the_terms(get_the_id(), 'feedwestafeed-locations');
            $main_article_image = get_the_post_thumbnail_url(get_the_id(), 'full');

            $result['html'] .= '<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 each_post_wrap">';
            $result['html'] .= '<div class="inner_each_post_wrap">';
            $result['html'] .= '<div class="mage_holder">';
            $result['html'] .= '<img src="';
            $result['html'] .= $main_article_image;
            $result['html'] .= '"';
            $result['html'] .= 'title="';
            $result['html'] .= get_the_title();
            $result['html'] .= '"';
            $result['html'] .= 'alt="';
            $result['html'] .= get_the_title();
            $result['html'] .= ' Image"/>';
            $result['html'] .= '</div>';
            $result['html'] .= '<div class="description">';
            $result['html'] .= '<ul>';
            $result['html'] .= '<li>';
            $result['html'] .= '<span class="cust_label">وصف: </span>';
            $result['html'] .= '<span class="cust_value">';
            $result['html'] .= get_the_content();
            $result['html'] .= '</span>';
            $result['html'] .= '</li>';
            $result['html'] .= '</ul>';
            $result['html'] .= '<div class="right_side">';
            $result['html'] .= '<ul>';
            $result['html'] .= '<li>';
            $result['html'] .= '<span class="cust_label">الاسم: </span>';
            $result['html'] .= '<span class="cust_value">';
            $result['html'] .= get_the_title();
            $result['html'] .= '</span>';
            $result['html'] .= '</li>';
            if ($category[0]->name) {
                $result['html'] .= '<li>';
                $result['html'] .= '<span class="cust_label">النشاط: </span>';
                $result['html'] .= '<span class="cust_value">';
                $result['html'] .= $category[0]->name;
                $result['html'] .= '</span>';
                $result['html'] .= '</li>';
            }
            if ($locations[0]->name) {
                $result['html'] .= '<li>';
                $result['html'] .= '<span class="cust_label">المكان: </span>';
                $result['html'] .= '<span class="cust_value">';
                $result['html'] .= $locations[0]->name;
                $result['html'] .= '</span>';
                $result['html'] .= '</li>';
            }
            $result['html'] .= '</ul>';
            $result['html'] .= '</div>';
            $result['html'] .= '<div class="left_side">';
            $result['html'] .= '<ul>';
            if ($feedwestafeed_phone_number) {
                $result['html'] .= '<li>';
                $result['html'] .= '<span class="cust_label">الهاتف: </span>';
                $result['html'] .= '<span class="cust_value"><a href="';
                $result['html'] .= $feedwestafeed_phone_number;
                $result['html'] .= '">';
                $result['html'] .= $feedwestafeed_phone_number;
                $result['html'] .= '+</a></span>';
                $result['html'] .= '</li>';
            }
            if ($feedwestafeed_website) {
                $result['html'] .= '<li>';
                $result['html'] .= '<span class="cust_label">الموقع: </span>';
                $result['html'] .= '<span class="cust_value"><a target="_blank" href="';
                $result['html'] .= $feedwestafeed_website;
                $result['html'] .= '">زيارة الموقع</a></span>';
                $result['html'] .= '</li>';
            }
            $result['html'] .= '</ul>';
            $result['html'] .= '</div>';
            $result['html'] .= '</div>';
            $result['html'] .= '</div>';
            $result['html'] .= '</div>';
        endwhile;

        $maximum_number_of_news = $first->max_num_pages;

        if (( $current_page + 1 ) > $maximum_number_of_news) {
            $news_is_last_page = true;
        } else {
            $result['html'] .= '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 get_more_btn">';
            $result['html'] .= '<a class="next-page custom_scroll" href="#"';
            $result['html'] .= 'data-count="2" data-link="';
            $result['html'] .= $link_infinite;
            $result['html'] .= '"';
            $result['html'] .= 'data-nonce="';
            $result['html'] .= $nonce_infinite;
            $result['html'] .= '">';
            $result['html'] .= '<span>المزيد</span>';
            $result['html'] .= '</a>';
            $result['html'] .= '<span class="loader">جاري التحميل...</span>';
            $result['html'] .= '</div>';
        }

    else:
        $result['html'] .= '<h3 class="noResultsFound">لا يوجد نتائج لبحثك</h3>';
    endif;

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $result = json_encode($result);
        echo $result;
    } else {
        header("Location: " . $_SERVER["HTTP_REFERER"]);
    }
    die();
}
