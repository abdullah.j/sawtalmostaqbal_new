/*
 * General js functions to be used !!SHOULD NOT BE ENQUEUED!!
 *
 * @package  TriTheme
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer  _REPLACE_WITH_DEV_NAME_
 */

/****************************************************************/
//initiate photoswiper
(function () {

    var initPhotoSwipeFromDOM = function (gallerySelector) {

        var parseThumbnailElements = function (el) {
            var thumbElements = el.childNodes,
                numNodes = thumbElements.length,
                items = [],
                el,
                childElements,
                thumbnailEl,
                size,
                item;

            for (var i = 0; i < numNodes; i++) {
                el = thumbElements[i];

                // include only element nodes
                if (el.nodeType !== 1) {
                    continue;
                }

                childElements = el.children;

                size = el.getAttribute('data-size').split('x');

                // create slide object
                item = {
                    src: el.getAttribute('href'),
                    w: parseInt(size[0], 10),
                    h: parseInt(size[1], 10),
                    author: el.getAttribute('data-author')
                };

                item.el = el; // save link to element for getThumbBoundsFn

                if (childElements.length > 0) {
                    item.msrc = childElements[0].getAttribute('src'); // thumbnail url
                    if (childElements.length > 1) {
                        item.title = childElements[1].innerHTML; // caption (contents of figure)
                    }
                }


                var mediumSrc = el.getAttribute('data-med');
                if (mediumSrc) {
                    size = el.getAttribute('data-med-size').split('x');
                    // "medium-sized" image
                    item.m = {
                        src: mediumSrc,
                        w: parseInt(size[0], 10),
                        h: parseInt(size[1], 10)
                    };
                }
                // original image
                item.o = {
                    src: item.src,
                    w: item.w,
                    h: item.h
                };

                items.push(item);
            }

            return items;
        };

        // find nearest parent element
        var closest = function closest(el, fn) {
            return el && (fn(el) ? el : closest(el.parentNode, fn));
        };

        var onThumbnailsClick = function (e) {
            e = e || window.event;
            e.preventDefault ? e.preventDefault() : e.returnValue = false;

            var eTarget = e.target || e.srcElement;

            var clickedListItem = closest(eTarget, function (el) {
                return el.tagName === 'A';
            });

            if (!clickedListItem) {
                return;
            }

            var clickedGallery = clickedListItem.parentNode;

            var childNodes = clickedListItem.parentNode.childNodes,
                numChildNodes = childNodes.length,
                nodeIndex = 0,
                index;

            for (var i = 0; i < numChildNodes; i++) {
                if (childNodes[i].nodeType !== 1) {
                    continue;
                }

                if (childNodes[i] === clickedListItem) {
                    index = nodeIndex;
                    break;
                }
                nodeIndex++;
            }

            if (index >= 0) {
                openPhotoSwipe(index, clickedGallery);
            }
            return false;
        };

        var photoswipeParseHash = function () {
            var hash = window.location.hash.substring(1),
                params = {};

            if (hash.length < 5) { // pid=1
                return params;
            }

            var vars = hash.split('&');
            for (var i = 0; i < vars.length; i++) {
                if (!vars[i]) {
                    continue;
                }
                var pair = vars[i].split('=');
                if (pair.length < 2) {
                    continue;
                }
                params[pair[0]] = pair[1];
            }

            if (params.gid) {
                params.gid = parseInt(params.gid, 10);
            }

            return params;
        };

        var openPhotoSwipe = function (index, galleryElement, disableAnimation, fromURL) {
            var pswpElement = document.querySelectorAll('.pswp')[0],
                gallery,
                options,
                items;

            items = parseThumbnailElements(galleryElement);

            // define options (if needed)
            options = {

                galleryUID: galleryElement.getAttribute('data-pswp-uid'),

                getThumbBoundsFn: function (index) {
                    // See Options->getThumbBoundsFn section of docs for more info
                    var thumbnail = items[index].el.children[0],
                        pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                        rect = thumbnail.getBoundingClientRect();

                    return {x: rect.left, y: rect.top + pageYScroll, w: rect.width};
                },

                addCaptionHTMLFn: function (item, captionEl, isFake) {
                    if (!item.title) {
                        captionEl.children[0].innerText = '';
                        return false;
                    }
                    captionEl.children[0].innerHTML = item.title;
                    return true;
                },

            };


            if (fromURL) {
                if (options.galleryPIDs) {
                    // parse real index when custom PIDs are used
                    // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
                    for (var j = 0; j < items.length; j++) {
                        if (items[j].pid == index) {
                            options.index = j;
                            break;
                        }
                    }
                } else {
                    options.index = parseInt(index, 10) - 1;
                }
            } else {
                options.index = parseInt(index, 10);
            }

            // exit if index not found
            if (isNaN(options.index)) {
                return;
            }


            var radios = document.getElementsByName('gallery-style');
            for (var i = 0, length = radios.length; i < length; i++) {
                if (radios[i].checked) {
                    if (radios[i].id == 'radio-all-controls') {

                    } else if (radios[i].id == 'radio-minimal-black') {
                        options.mainClass = 'pswp--minimal--dark';
                        options.barsSize = {top: 0, bottom: 0};
                        options.captionEl = false;
                        options.fullscreenEl = false;
                        options.shareEl = false;
                        options.bgOpacity = 0.85;
                        options.tapToClose = true;
                        options.tapToToggleControls = false;
                    }
                    break;
                }
            }

            if (disableAnimation) {
                options.showAnimationDuration = 0;
            }

            // Pass data to PhotoSwipe and initialize it
            gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);

            // see: http://photoswipe.com/documentation/responsive-images.html
            var realViewportWidth,
                useLargeImages = false,
                firstResize = true,
                imageSrcWillChange;

            gallery.listen('beforeResize', function () {

                var dpiRatio = window.devicePixelRatio ? window.devicePixelRatio : 1;
                dpiRatio = Math.min(dpiRatio, 2.5);
                realViewportWidth = gallery.viewportSize.x * dpiRatio;


                if (realViewportWidth >= 1200 || (!gallery.likelyTouchDevice && realViewportWidth > 800) || screen.width > 1200) {
                    if (!useLargeImages) {
                        useLargeImages = true;
                        imageSrcWillChange = true;
                    }

                } else {
                    if (useLargeImages) {
                        useLargeImages = false;
                        imageSrcWillChange = true;
                    }
                }

                if (imageSrcWillChange && !firstResize) {
                    gallery.invalidateCurrItems();
                }

                if (firstResize) {
                    firstResize = false;
                }

                imageSrcWillChange = false;

            });

            gallery.listen('gettingData', function (index, item) {
                if (useLargeImages) {
                    item.src = item.o.src;
                    item.w = item.o.w;
                    item.h = item.o.h;
                } else {
                    item.src = item.m.src;
                    item.w = item.m.w;
                    item.h = item.m.h;
                }
            });

            gallery.init();
        };

        // select all gallery elements
        var galleryElements = document.querySelectorAll(gallerySelector);
        for (var i = 0, l = galleryElements.length; i < l; i++) {
            galleryElements[i].setAttribute('data-pswp-uid', i + 1);
            galleryElements[i].onclick = onThumbnailsClick;
        }

        // Parse URL and open gallery if it contains #&pid=3&gid=1
        var hashData = photoswipeParseHash();
        if (hashData.pid && hashData.gid) {
            openPhotoSwipe(hashData.pid, galleryElements[hashData.gid - 1], true, true);
        }
    };

    initPhotoSwipeFromDOM('_PUT_CLASS_HERE');

})();

/****************************************************************/
//initiate simple weather

//load of weather api script
loadWeather('_COUNTRY_NAME_HERE_', ''); //@params location, woeid
function loadWeather(location, woeid) {
    jQuery.simpleWeather({
        location: location,
        woeid: woeid,
        unit: 'c',
        success: function (weather) {
            var templateDir = "<?php bloginfo('template_directory') ?>";
            html = '<h2 data-country="' + weather.city + '"><img src="' + theme_directory + '/images/weather/' + weather.code + '.png"> ' + weather.temp + '&deg;</h2>';

            for (var i = 0; i < 5; i++) {
                if (i == 3 || i == 4) {
                    html += '<span class="hidden_on_mobile"><img class="mini_image" src="' + theme_directory + '/images/weather/' + weather.forecast[i].code + '.png"><p>' + weather.forecast[i].day + ': ' + weather.forecast[i].high + '&deg;</p></span>';
                } else {
                    html += '<span><img class="mini_image" src="' + theme_directory + '/images/weather/' + weather.forecast[i].code + '.png"><p>' + weather.forecast[i].day + ': ' + weather.forecast[i].high + '&deg;</p></span>';
                }
            }
            jQuery("_YOUR_CLASS_HERE_").html(html);
        },
        error: function (error) {
            jQuery("_YOUR_CLASS_HERE_").html('<p class="weather-error">' + error + '<a href="/">Please refresh</a></p>');
        }
    });
}

//Initiate simple weather accordint to IP
jQuery.get("http://freegeoip.net/json/", function (response) {
    var location = response.city;
//        alert(location);

    if (location !== '') {
        loadWeather(location, '');
    } else {
        loadWeather('Beirut', '');
    }
}, "jsonp");

//load of weather api script
function loadWeather(location, woeid) {
    jQuery.simpleWeather({
        location: location,
        woeid: woeid,
        unit: 'c',
        success: function (weather) {
            var templateDir = "<?php bloginfo('template_directory') ?>";
            html = '<h2>' + weather.temp + '&deg; &nbsp; <img class="weather_icon" src="' + theme_directory + '/images/weather/' + weather.code + '.png"></h2>';
            jQuery("#weather").html(html);
        },
        error: function (error) {
            jQuery("#weather").html('<p class="weather-error">' + error + '</p>');
        }
    });
}

/****************************************************************/
//clean lists dots
function clean(node)
{
    for (var n = 0; n < node.childNodes.length; n++)
    {
        var child = node.childNodes[n];
        if
        (
            child.nodeType === 8
            ||
            (child.nodeType === 3 && !/\S/.test(child.nodeValue))
        )
        {
            node.removeChild(child);
            n--;
        } else if (child.nodeType === 1)
        {
            clean(child);
        }
    }
}

clean(document);

/****************************************************************/
//sticky image with scroll

$(window).load(function () {
    var element = $('.to_follow_scroll');
    if (element.length) {
        var originalY = element.offset().top;

        var topMargin = 20;

        element.addClass('follow-scroll');

        $(window).bind('scroll', function () {
            var scrollTop = $(window).scrollTop();
            if ((scrollTop < originalY)) {
                element.parent().find('.posts').css({'float': 'left'});
                element.css({'position': 'unset'});
            } else if ((scrollTop + $(window).height() < $(document).height())) {
                element.parent().find('.posts').css({'float': 'right'});
                element.css({'position': 'fixed', 'top': '15px'});
            } else {
                element.parent().find('.posts').css({'float': 'right'});
                element.css({'position': 'fixed', 'top': '-120px'});
            }
        });
    }
});

/****************************************************************/
//initiate jPush

$('.toggle-menu').jPushMenu(
    {
        closeOnClickOutside: false,
    }
);

/****************************************************************/
//initiate swiper

/*with pagination*/
var mySwiper = new Swiper('.swiper-container.with_pag.tablet_slider', {
    loop: true,
    pagination: '.swiper-pagination.with_pag',
    paginationClickable: true,
    slidesPerView: 2,
    centeredSlides: true,
    spaceBetween: 18,
});

/*without pagination*/
var mySwiper = new Swiper('.swiper-container.without_pag', {
    loop: true,
    slidesPerView: 2,
    centeredSlides: true,
    spaceBetween: 12,
});

/****************************************************************/
//add link behavior

jQuery('.link_behavior').each(function () {
    var link = $(this).attr('data-url');
    $(this).wrapInner('<a href="' + link + '" />');
});

/****************************************************************/
//magnific popup

$('.gal').magnificPopup({
    delegate: 'a.gal_link',
    type: 'image',
    gallery: {
        enabled: true
    },
    image: {
        titleSrc: function (item) {
            return item.el.attr('title');
        }
    }
});

/****************************************************************/
//jscroll infinite scroll

jQuery('.posts-container').jscroll({
    loadingHtml: '<span class="loader"> جاري التحميل...</span>',
    padding: 20,
    nextSelector: '.navigation a',
    contentSelector: '.posts-container',
    autoTrigger: false,
    callback: function () {
        jQuery('.link_behavior').each(function () {
            var link = $(this).attr('data-url');
            $(this).wrapInner('<a href="' + link + '" />');
        });
    }
});

/****************************************************************/
//font resize

jQuery(document).on('click', '.fontresize', function (e) {
    e.preventDefault();
    jQuery(this).parent().find('#restore').show();
    jQuery(this).parent().find('#minus').show();
    var id = jQuery(this).attr('id');
    var fontsize = parseInt(jQuery(this).parent().parent().parent().find('.content-holder p').css("font-size"));
    var lineheight = parseInt(jQuery(this).parent().parent().parent().find('.content-holder p').css("line-height"));
    if (fontsize < 13 || fontsize > 40) {
        fontsize = 13 + "px";
        lineheight = 30 + "px";
        jQuery(this).parent().find('#restore').hide();
        jQuery(this).parent().find('#minus').hide();
    } else {
        if (id == "plus") {
            fontsize = fontsize + 1 + "px";
            lineheight = lineheight + 1.5 + "px";
        } else if (id == "minus") {
            fontsize = fontsize - 1 + "px";
            lineheight = lineheight - 1.5 + "px";
        } else if (id == "restore") {
            fontsize = 13 + "px";
            lineheight = 30 + "px";
            jQuery(this).parent().find('#restore').hide();
            jQuery(this).parent().find('#minus').hide();
        }
    }
    jQuery(this).parent().parent().parent().find('.content-holder p').css({"font-size": fontsize});
    jQuery(this).parent().parent().parent().find('.content-holder p').css({"line-height": lineheight});
});

/****************************************************************/
//print

jQuery(document).on('click', '.print a', function (e) {
    e.preventDefault();
    window.print();
});

/****************************************************************/
//time ago initiator
jQuery("time.timeago").timeago();

/****************************************************************/
//range slider initiator

jQuery(document).ready(function () {
//creation of the advanced range sider
    var nonLinearSlider = document.getElementById('nonlinear');

    noUiSlider.create(nonLinearSlider, {
        connect: true,
        tooltips: true,
        behaviour: 'tap',
        step: 1,
        start: [ 0, 7100000 ],
        range: {
            // Starting at 500, step the value by 500,
            // until 4000 is reached. From there, step by 1000.
            'min': [ 0 ],
            '1%': [ 10000,10000 ],
            '50%': [ 1000000, 100000 ],
            'max': [ 7100000 ]
        },
        format:
            wNumb({
                decimals: 0,
                thousand:',',
                postfix: ''
            })
    });
// Write the CSS 'left' value to a span.
    function leftValue ( handle ) {
        return handle.parentElement.style.left;
    }
    var lowerValue = document.getElementById('lower-value'),
        lowerOffset = document.getElementById('lower-offset'),
        upperValue = document.getElementById('upper-value'),
        upperOffset = document.getElementById('upper-offset'),
        handles = nonLinearSlider.getElementsByClassName('noUi-handle');
// Display the slider value and how far the handle moved
// from the left edge of the slider.
    nonLinearSlider.noUiSlider.on('update', function ( values, handle ) {
        if ( !handle ) {
            lowerValue.innerHTML = values[handle] + '$';
            var value1 = values[handle];
            jQuery('#lower-value').attr('value', values[handle].replace(/,/g , ""));
        } else {
            upperValue.innerHTML = values[handle] + '$';
            jQuery('#upper-value').attr('value', values[handle].replace(/,/g , ""));
        }
    });
    nonLinearSlider.noUiSlider.on('update', function () {
        var valueupper = jQuery('#upper-value').attr('value');
        var valuelower = jQuery('#lower-value').attr('value');
        var valueupper2 = numeral(valueupper).format('0.0a');
        var valuelower2 = numeral(valuelower).format('0.0a');
        jQuery('.noUi-handle-upper .noUi-tooltip').html(valueupper2);
        jQuery('.noUi-handle-lower .noUi-tooltip').html(valuelower2);
    });
});
/****************************************************************/
//CSS variables example

/*html {
    --primeColor: red;
}


 read more:

 https://una.im/local-css-vars/

 https://www.broken-links.com/2014/08/28/css-variables-updating-custom-properties-javascript/

 Demo:

 https://codepen.io/wesbos/pen/adQjoY

 https://googlechrome.github.io/samples/css-custom-properties/index.html

.section {
    background-color: var(--primeColor);
}*/

var html = document.getElementsByTagName('html')[0];
html.style.cssText = "--main-background-color: red";


var html = document.getElementsByTagName('html')[0];
html.style.setProperty("--main-background-color", "green");


var html = document.getElementsByTagName('html')[0];
html.setAttribute("style", "--main-background-color: green");

/****************************************************************/