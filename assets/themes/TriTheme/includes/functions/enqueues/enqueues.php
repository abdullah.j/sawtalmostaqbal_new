<?php

/**
 * All Enqueues
 *
 * @package  TriTheme
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer  _REPLACE_WITH_DEV_NAME_
 */

/**
 * enqueue js files
 */
function TriTheme_scripts() {
    wp_enqueue_script(
            'jQuery', get_template_directory_uri() . '/libraries/jQuery/jquery.min.js', array('jquery')
    );
    wp_enqueue_script(
            'TriTheme', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.01'
    );
    wp_enqueue_script(
            'swiper', get_template_directory_uri() . '/libraries/general_libraries/swiper/swiper.min.js', array('jquery')
    );
    wp_enqueue_script(
            'jscroll', get_template_directory_uri() . '/libraries/general_libraries/jscroll/jscroll.js', array('jquery')
    );
    wp_enqueue_script(
            'iscroll', get_template_directory_uri() . '/libraries/general_libraries/iscroll/iscroll.js', array('jquery')
    );
    wp_enqueue_script(
        'player', get_template_directory_uri() . '/js/player.min.js', array('jquery'), '1.0'
    );
    wp_enqueue_script(
        'scrollbar', get_template_directory_uri() . '/libraries/general_libraries/nicescroll/jquery.nicescroll.min.js', array('jquery')
    );
    wp_enqueue_script(
        'hammer', get_template_directory_uri() . '/libraries/general_libraries/hammer/hammer.js', array('jquery')
    );
}

add_action('wp_enqueue_scripts', 'TriTheme_scripts');

/**
 * enqueue css files
 */
function TriTheme_styles() {
    wp_enqueue_style(
            'font-awsome_css', get_template_directory_uri() . '/libraries/font-awesome/css/font-awesome.css', array(), '1.0'
    );
    wp_enqueue_style(
            'bootstrap_css', get_template_directory_uri() . '/libraries/bootstrap/css/bootstrap.min.css', array(), '1.0'
    );
    wp_enqueue_style(
            'reset', get_template_directory_uri() . '/css/reset.css', array(), '1.00'
    );
    wp_enqueue_style(
            'styles', get_template_directory_uri() . '/css/styles.css', array(), '1.01'
    );
    wp_enqueue_style(
            'swiper', get_template_directory_uri() . '/libraries/general_libraries/swiper/swiper.min.css', array(), '1.0'
    );
    wp_enqueue_style(
            'styles-1600px', get_template_directory_uri() . '/css/styles-1600px.css', array(), '1.01', 'screen and (max-width:1600px)'
    );
    wp_enqueue_style(
            'styles-1500px', get_template_directory_uri() . '/css/styles-1500px.css', array(), '1.01', 'screen and (max-width:1500px)'
    );
    wp_enqueue_style(
            'styles-1400px', get_template_directory_uri() . '/css/styles-1400px.css', array(), '1.01', 'screen and (max-width:1400px)'
    );
    wp_enqueue_style(
            'styles-1300px', get_template_directory_uri() . '/css/styles-1300px.css', array(), '1.01', 'screen and (max-width:1300px)'
    );
    wp_enqueue_style(
            'styles-1200px', get_template_directory_uri() . '/css/styles-1200px.css', array(), '1.01', 'screen and (max-width:1200px)'
    );
    wp_enqueue_style(
            'styles-992px', get_template_directory_uri() . '/css/styles-992px.css', array(), '1.01', 'screen and (max-width:992px)'
    );
    wp_enqueue_style(
            'styles-768px', get_template_directory_uri() . '/css/styles-768px.css', array(), '1.01', 'screen and (max-width:768px)'
    );
    wp_enqueue_style(
            'styles-500px', get_template_directory_uri() . '/css/styles-500px.css', array(), '1.01', 'screen and (max-width:500px)'
    );
    wp_enqueue_style(
            'styles-400px', get_template_directory_uri() . '/css/styles-400px.css', array(), '1.01', 'screen and (max-width:400px)'
    );
    wp_enqueue_style(
            'styles-300px', get_template_directory_uri() . '/css/styles-300px.css', array(), '1.01', 'screen and (max-width:300px)'
    );
}

add_action('wp_enqueue_scripts', 'TriTheme_styles');

