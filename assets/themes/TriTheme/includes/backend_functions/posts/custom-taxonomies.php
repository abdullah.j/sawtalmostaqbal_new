<?php

/**
 * Custom Taxonomies Registration
 *
 * @package  TriTheme
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer  _REPLACE_WITH_DEV_NAME_
 */
function feedwestafeed_taxonomy_location() {
    $labels = array(
        'name' => 'Locations',
        'singular_name' => 'Location',
        'search_items' => 'Search Locations',
        'all_items' => 'All Locations',
        'edit_item' => 'Edit Location',
        'update_item' => 'Update Location',
        'add_new_item' => 'Add New Location',
        'new_item_name' => 'New Location',
        'menu_name' => 'Locations',
        'view_item' => 'View Location',
        'popular_items' => 'Popular Location',
        'separate_items_with_commas' => 'Separate Locations with commas',
        'add_or_remove_items' => 'Add or remove Location',
        'choose_from_most_used' => 'Choose from the most used Locations',
        'not_found' => 'No Locations found'
    );

    register_taxonomy(
            'feedwestafeed-locations', 'feed_w_estafeed', array(
        'label' => __('Locations'),
        'hierarchical' => true,
        'labels' => $labels,
        'public' => true,
        'show_in_nav_menus' => false,
        'show_tagcloud' => false,
        'show_admin_column' => true,
        'rewrite' => array(
            'slug' => 'feedwestafeed-locations'
        )
            )
    );
}

add_action('init', 'feedwestafeed_taxonomy_location');

function feedwestafeed_taxonomy_category() {
    $labels = array(
        'name' => 'Categories',
        'singular_name' => 'Category',
        'search_items' => 'Search Categories',
        'all_items' => 'All Categories',
        'edit_item' => 'Edit Category',
        'update_item' => 'Update Category',
        'add_new_item' => 'Add New Category',
        'new_item_name' => 'New Category',
        'menu_name' => 'Categories',
        'view_item' => 'View Category',
        'popular_items' => 'Popular Category',
        'separate_items_with_commas' => 'Separate Categories with commas',
        'add_or_remove_items' => 'Add or remove Category',
        'choose_from_most_used' => 'Choose from the most used Categories',
        'not_found' => 'No Categories found'
    );

    register_taxonomy(
            'feedwestafeed-categories', 'feed_w_estafeed', array(
        'label' => __('Locations'),
        'hierarchical' => true,
        'labels' => $labels,
        'public' => true,
        'show_in_nav_menus' => false,
        'show_tagcloud' => false,
        'show_admin_column' => true,
        'rewrite' => array(
            'slug' => 'feedwestafeed-categories'
        )
            )
    );
}

add_action('init', 'feedwestafeed_taxonomy_category');

//function sample_taxonomy_tags() {
//	$labels = array(
//		'name'                           => 'Sample Tags',
//		'singular_name'                  => 'Sample tag',
//		'search_items'                   => 'Search Sample Tags',
//		'all_items'                      => 'All Sample Tags',
//		'edit_item'                      => 'Edit Sample Tag',
//		'update_item'                    => 'Update Sample Tag',
//		'add_new_item'                   => 'Add New Sample Tag',
//		'new_item_name'                  => 'New Sample Tag',
//		'menu_name'                      => 'Sample Tags',
//		'view_item'                      => 'View Sample Tag',
//		'popular_items'                  => 'Popular Sample Tag',
//		'separate_items_with_commas'     => 'Separate Sample Tags with commas',
//		'add_or_remove_items'            => 'Add or remove Sample Tag',
//		'choose_from_most_used'          => 'Choose from the most used Sample Tags',
//		'not_found'                      => 'No Sample Tags found'
//	);
//
//	register_taxonomy(
//		'sample-tags',
//		'post_slug',
//		array(
//			'label' => __( 'Sample Tags' ),
//			'hierarchical' => false,
//			'labels' => $labels,
//			'public' => true,
//			'show_in_nav_menus' => false,
//			'show_tagcloud' => false,
//			'show_admin_column' => true,
//			'rewrite' => array(
//				'slug' => 'sample-tags'
//			)
//		)
//	);
//}
//add_action( 'init', 'sample_taxonomy_tags' );