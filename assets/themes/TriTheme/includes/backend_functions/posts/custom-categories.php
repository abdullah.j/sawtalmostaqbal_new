<?php
/**
 * Custom categories registration
 *
 * @package  TriTheme
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer  _REPLACE_WITH_DEV_NAME_
 */
//function create_sample_taxonomy()
//{
//    $labels = array(
//        'name' => 'Sample Categories',
//        'singular_name' => 'sample-cats',
//        'search_items' => 'Search Sample Categories',
//        'all_items' => 'All Sample Categories',
//        'edit_item' => 'Edit Sample Category',
//        'update_item' => 'Update Sample Category',
//        'add_new_item' => 'Add New Sample Category',
//        'new_item_name' => 'New Sample Category',
//        'menu_name' => 'Sample Categories',
//        'view_item' => 'View Sample Category',
//        'popular_items' => 'Popular Sample Category',
//        'separate_items_with_commas' => 'Separate Sample Categories with commas',
//        'add_or_remove_items' => 'Add or remove Sample Categories',
//        'choose_from_most_used' => 'Choose from the most used Sample Categories',
//        'not_found' => 'No Sample Categories found'
//    );
//
//    register_taxonomy(
//        'sample-cats', 'post_slug', array(
//            'label' => __('Sample Categories'),
//            'hierarchical' => true,
//            'labels' => $labels,
//            'public' => true,
//            'show_in_nav_menus' => false,
//            'show_tagcloud' => false,
//            'show_admin_column' => true,
//            'rewrite' => array(
//                'slug' => 'sample-cats'
//            )
//        )
//    );
//}
//add_action('init', 'create_sample_taxonomy');