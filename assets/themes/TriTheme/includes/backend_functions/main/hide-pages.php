<?php
/**
 * Hide pages for non admins
 *
 * @package  TriTheme
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer  _REPLACE_WITH_DEV_NAME_
 */
//function exclude_pages_from_admin($query) {
//    if ( ! is_admin() )
//        return $query;
//    global $pagenow, $post_type;
//
//    if ( !current_user_can( 'administrator' ) && $pagenow == 'edit.php' && $post_type == 'page' )
//        /*Add pages IDs goes here*/
//        $query->query_vars['post__not_in'] = array('');
//}
//add_filter( 'parse_query', 'exclude_pages_from_admin' );


