<?php
/**
 * Template name: Home
 *
 * @package  TriTheme
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer  Marc bou sleiman <http://marcbousleiman.com>
 */
get_header();
$template_url = get_bloginfo('template_url');
?>
<div class="homepage padding_top">
    <div class="top_slider_section">
        <div class="swiper-container s_top_section">
            <div class="nextSection">
                <a href="#programs_schedule_id"><div>شاهد الأن</div><img src="<?php echo $template_url; ?>/images/buttonDown.png" alt="Icon"></a>
            </div>
            <div class="swiper-wrapper">
                <?php
                if (have_rows('homepage_slider', 'option')):
                    $sliders_counter = 1;
                    while (have_rows('homepage_slider', 'option')) : the_row();

                        $program_slide_or_custom_slide = get_sub_field('program_slide_or_custom_slide', 'option');

                        if ($program_slide_or_custom_slide == 'program_slide') {
                            $slider_program = get_sub_field('slide_program', 'option');
                            $program_logo = get_field('program_logo', $slider_program);
                            $image_sup = wp_get_attachment_image_src($program_logo, 'homepage_slider_prg_logo');
                            $slide_image = get_the_post_thumbnail_url($slider_program, 'full');
                            $slider_link = get_the_permalink($slider_program);
                            ?>
                            <div class="dont_open swiper-slide" style="background-image: url(<?php echo $slide_image; ?>)">
                                <div class="inner_description">
                                    <img src="<?php echo $image_sup[0]; ?>" alt="Program logo" title="<?php echo get_the_title($slider_program); ?>">
                                    <a href="<?php echo $slider_link; ?>">شاهد الحلقات</a>
                                </div>
                            </div>
                            <?php
                        } else {
                            $homepage_slider_custom_slide_image = get_sub_field('homepage_slider_custom_slide_image');
                            $homepage_slider_custom_slide_image_sup = wp_get_attachment_image_src($homepage_slider_custom_slide_image, 'full');
                            $homepage_slider_custom_slide_description = get_sub_field('homepage_slider_custom_slide_description');
                            $homepage_slider_custom_slide_button_label = get_sub_field('homepage_slider_custom_slide_button_label');
                            $homepage_slider_custom_side_button_url = get_sub_field('homepage_slider_custom_side_button_url');
                            ?>
                            <div class="dont_open swiper-slide" style="background-image: url(<?php echo $homepage_slider_custom_slide_image_sup[0]; ?>)">
                                <div class="inner_description">
                                    <h3><?php echo $homepage_slider_custom_slide_description; ?></h3>
                                    <?php if ($homepage_slider_custom_slide_button_label && $homepage_slider_custom_side_button_url) { ?>
                                        <a href="<?php echo $homepage_slider_custom_side_button_url; ?>"><?php echo $homepage_slider_custom_slide_button_label; ?></a>
                                    <?php } ?>
                                    <div class="swiper-pagination s_top_section_pag"></div>
                                </div>
                            </div>
                            <?php
                        }
                        $sliders_counter++;
                    endwhile;
                endif;
                ?>
            </div>
        </div>
    </div>
    <div id="programs_schedule_id" class="programs_schedule">
        <h1 class="section_identifier">جدول البرامج</h1>
        <div class="programs_wrapper">
            <div dir="rtl" class="swiper-container s_programs_section">
                <div class="swiper-wrapper">
                    <?php
                    $args = array(
                        'post_type' => 'programs',
                        'posts_per_page' => 8,
                        'order' => 'asc',
                        'post_status' => 'publish'
                    );

                    $first = new WP_Query($args);

                    if ($first->have_posts()):
                        while ($first->have_posts()):
                            $first->the_post();
                            $program_time = get_field('program_time');
                            ?>
                            <div class="swiper-slide">
                                <a href="<?php echo get_the_permalink(); ?>">
                                    <div class="image_holder">
                                        <?php echo the_post_thumbnail('homepage_programs', array('class' => 'each_program_img', 'alt' => get_the_title(), 'title' => get_the_title()));
                                        ?>
                                        <div class="date_wrapper">
                                            <h4><?php echo $program_time; ?></h4>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <?php
                        endwhile;
                    endif;
                    ?>
                </div>
                <!--Add Arrows -->
                <div class = "swiper-button-next s_programs_section_nav">
                    <i class = "fa fa-angle-right" aria-hidden = "true"></i>
                </div>
                <div class = "swiper-button-prev s_programs_section_nav">
                    <i class = "fa fa-angle-left" aria-hidden = "true"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="featured_programs_section">
        <div class="container-fluid no_padding">
            <div class="right_section col-lg-7 col-md-6 col-sm-4 col-xs-4">
                <?php
                $args2 = array(
                    'post_type' => 'programs',
                    'posts_per_page' => 1,
                    'order' => 'asc',
                    'post_status' => 'publish',
                    'meta_query' => array(
                        array(
                            'key' => 'program_on_homepage',
                            'compare' => '==',
                            'value' => '1'
                        )
                    )
                );

                $first2 = new WP_Query($args2);

                if ($first2->have_posts()):
                    while ($first2->have_posts()):
                        $first2->the_post();
                        $program_time = get_field('program_time');
                        $background_image = get_the_post_thumbnail_url(get_the_id(), 'homepage_large_program_img');
                        ?>
                        <div class="inner_program_wrapper" style="background-image : url('<?php echo $background_image; ?>')">
                            <div class="inner_description">
                                <a href="<?php echo get_the_permalink(); ?>">
                                    <h1><?php echo get_the_title(); ?></h1>
                                    <h3><?php echo $program_time; ?></h3>
                                    <div class="clear shown_on_monile"></div>
                                    <img class="play_button" title="Play" alt="Play" src="<?php echo get_template_directory_uri(); ?>/images/play-button.png">
                                </a>
                            </div>
                        </div>
                        <?php
                    endwhile;
                endif;
                ?>
            </div>
            <div class="left_section col-lg-5 col-md-6 col-sm-8 col-xs-8">
                <div class="programs_episodes_wrapper">
                    <?php
                    $args_episodes = array(
                        'post_type' => 'programs_episodes',
                        'posts_per_page' => 6,
                        'order' => 'DESC',
                        'post_status' => 'publish',
                        'meta_query' => array(
                            array(
                                'key' => 'episode_on_homepage',
                                'compare' => '==',
                                'value' => '1'
                            )
                        )
                    );

                    $first_episodes = new WP_Query($args_episodes);

                    if ($first_episodes->have_posts()):
                        while ($first_episodes->have_posts()):
                            $first_episodes->the_post();
                            $parent_program = get_field('episode_program');
                            $program_time = get_field('program_time', $parent_program);
                            ?>
                            <div class="each_episode_wrapper">
                                <a href="<?php echo get_the_permalink(); ?>">
                                    <div class="right_content">
                                        <div class="image_holder">
                                            <?php echo the_post_thumbnail('homepage_small_episode_img', array('class' => 'each_program_img', 'alt' => get_the_title(), 'title' => get_the_title()));
                                            ?>
                                        </div>
                                        <div class="title_holder">
                                            <h3><?php custom_length(get_the_title(), 40); ?></h3>
                                            <h4><?php echo get_the_title($parent_program); ?></h4>
                                        </div>
                                    </div>
                                    <div class="left_content">
                                        <span class="arrow_init"><i class = "fa fa-angle-left" aria-hidden = "true"></i></span>
                                        <h4><?php custom_length($program_time, 50); ?></h4>
                                    </div>
                                    <div class="clear"></div>
                                </a>
                            </div>
                            <?php
                        endwhile;
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom_banner_section">
        <div class="container-fluid">
            <?php
            $imageleader_bottom = get_field('home_middle_banner_image_adsense', 'option');
            $imageurlleader_bottom = get_field('home_middle_banner_link_adsense', 'option');
            $banner_scriptleader_bottom = get_field('home_middle_banner_script_adsense', 'option');
            $banner_scriptleader_mobile_bottom = get_field('home_middle_banner_script_adsense_mobile', 'option');
            if (!empty($imageleader_bottom) && !empty($imageurlleader_bottom)) {
                ?>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 leaderboard-ad-wrapper">
                    <div class="leaderboard-ads">
                        <a href="<?php echo $imageurlleader_bottom; ?>" target="_blank"><img src="<?php echo $imageleader_bottom['url']; ?>" alt="<?php echo $imageleader_bottom['alt']; ?>" /></a>
                    </div>
                </div>
            <?php } else if (!empty($banner_scriptleader_bottom)) {
                ?>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 leaderboard-ad-wrapper">
                    <div class="leaderboard-ads ads_on_desktop">
                        <?php echo $banner_scriptleader_bottom; ?>
                    </div>
                    <div class="leaderboard-ads ads_on_mobile">
                        <?php echo $banner_scriptleader_mobile_bottom; ?>
                    </div>
                </div>
            <?php }
            ?>
        </div>
    </div>
</div>
<script type = "text/javascript">
    var height = jQuery(window).height();
    var top_header_height = jQuery('.top_header').height();
    var width = jQuery(window).width();
    if (width > 500) {
        jQuery('.homepage .top_slider_section').height(height - (top_header_height + 150));
    } else {
        jQuery('.homepage .top_slider_section').height(height - (top_header_height + 50));
    }

    var slides_per_view = 3;
    var space_between = 20;

    if (width < 992) {
        slides_per_view = 2.5;
    }

    if (width < 500) {
        slides_per_view = 1.2;
    }

    jQuery(window).load(function () {
        var width = jQuery(window).width();
        if (width > 768) {
            //featured programs sections height
            var left_Section_h = jQuery('.homepage .featured_programs_section .left_section').height();
            jQuery('.homepage .featured_programs_section .right_section .inner_program_wrapper').height(left_Section_h);
        }
    });

    var s_programs_section = new Swiper('.swiper-container.s_programs_section', {
        navigation: {
            nextEl: '.swiper-button-prev.s_programs_section_nav',
            prevEl: '.swiper-button-next.s_programs_section_nav'
        },
        slidesPerView: slides_per_view,
        spaceBetween: space_between
    });

    jQuery('.homepage .poll_section .poll-holder form .wp-polls-ans p:nth-child(2) input').attr('value', 'صوت');
    jQuery('.homepage .poll_section .poll-holder form .wp-polls-ans p:nth-child(3) a').html('النتائج');

    setInterval(function () {
        var stream_title = jQuery('.sp-information .sp-details span.sp-title').html();
        var stream_artist = jQuery('.sp-information .sp-details span.sp-artist').html();

        jQuery('.homepage .live_stream_section .right_section .inner_right_section .live_air_title_desc h1').html(stream_title);
        jQuery('.homepage .live_stream_section .right_section .inner_right_section .live_air_title_desc h3').html(stream_artist);
    }, 5000);

    jQuery('.homepage .live_stream_section .left_section .image_holder img.pause_button').on('click', function () {
        jQuery('.homepage .live_stream_section').removeClass('playing');
        jQuery('#sp-audio').get(0).pause();
    });

    jQuery('.homepage .live_stream_section .left_section .image_holder img.play_button').on('click', function () {
        jQuery('.homepage .live_stream_section').addClass('playing');
        jQuery('#sp-audio').get(0).play();
    });

    jQuery('.homepage .live_stream_section .right_section .inner_right_section .air_identification').on('click', function () {

        if (jQuery('.homepage .live_stream_section').hasClass('playing')) {
            jQuery('.homepage .live_stream_section').removeClass('playing');
            jQuery('#sp-audio').get(0).pause();
        } else {
            jQuery('.homepage .live_stream_section').addClass('playing');
            jQuery('#sp-audio').get(0).play();
        }
    });
</script>
<?php
get_footer();
