<?php
/**
 * Default archive genre categories template
 *
 * @package  TriTheme
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer  Marc bou sleiman <http://marcbousleiman.com>
 */
get_header();
$queried_object = get_queried_object();
$genre_id = $queried_object->term_id;
$genre_name = $queried_object->name;
?>
<?php
get_footer();
