<?php
/*
 * 
 * Template name: Contact us
 * 
 */
get_header();
?>
<div class="contact_us_page padding_top">
<!--    <div class="content">
        <div class="container-fluid">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_identifier">
                <h1>تواصل معنا</h1>
            </div>
            <?php
            $email = get_field('email', 'options');
            $address = get_field('address', 'options');
            $phone = get_field('phone_number', 'options');
            ?>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 left_section parent_columns">
                <div class="inner">
                    <a class="info_link" href="mailto:<?php echo $email; ?>"><i class="fa fa-envelope" aria-hidden="true"></i></a>
                    <a class="info myriad" href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 middle_section parent_columns">
                <div class="inner">
                    <a class="info_link" href="tel:<?php echo $phone; ?>"><i class="fa fa-phone" aria-hidden="true"></i></a>
                    <a class="info" href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 right_section parent_columns">
                <div class="inner">
                    <a class="info_link" href="#"><i class="fa fa-location-arrow" aria-hidden="true"></i></a>
                    <a class="info" href="#"><?php echo $address; ?></a>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 leave_a_msg_wrapper">
                <span class="init_contact_form">أترك لنا رسالة</span>
            </div>
        </div>
    </div>-->
    <div class="content_form_wrapper">
        <div class="container-fluid">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_identifier">
                <!--<img class="exit_btn" src="<?php // echo get_template_directory_uri(); ?>/images/x-logo-white.png" title="Exit" alt="Exit">-->
                <h1>أترك لنا رسالة</h1>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form_wrapper">
                <?php echo do_shortcode('[contact-form-7 id="4" title="Contact form 1"]'); ?>
            </div>
        </div>
    </div>
    <div class="form_responses_holder">
        <h1></h1>
        <a href="<?php echo esc_url(home_url('/contact-us')); ?>">إعادة تحميل الصفحة؟</a>
    </div>
</div>
<script type = "text/javascript">
    jQuery(window).load(function () {
        var height = jQuery(window).height();
        var top_header_height = jQuery('.top_header').height();
        var bottom_player_height = jQuery('.footer').height() + 30;
        var top_plus_bottom = top_header_height + bottom_player_height;
        
            jQuery('.contact_us_page').css('min-height', height );
    });
    //on click send us a message
    jQuery('.contact_us_page .content .leave_a_msg_wrapper span.init_contact_form').on('click', function () {
        jQuery('.contact_us_page .content').fadeOut(500, function () {
            jQuery('.contact_us_page .content_form_wrapper').fadeIn(500);
        });
    });
    //on exit button click close form
    jQuery('.contact_us_page .content_form_wrapper .section_identifier img.exit_btn').on('click', function () {
        jQuery('.contact_us_page .content_form_wrapper').fadeOut(500, function () {
            jQuery('.contact_us_page .content').fadeIn(500);
        });
    });
    
    jQuery(document).on('wpcf7:mailsent', function () {
        jQuery('.contact_us_page .form_responses_holder h1').html('نشكركم على الاتصال بنا! سوف نعود اليكم قريبا.');
        jQuery('.contact_us_page .content_form_wrapper').fadeOut(500, function () {
            jQuery('.contact_us_page .form_responses_holder').fadeIn(500);
        });
    });

    jQuery(document).on('wpcf7:mailfailed', function () {
        jQuery('.contact_us_page .form_responses_holder h1').html('كان هناك خطأ! حاول مرة اخرى.');
        jQuery('.contact_us_page .content_form_wrapper').fadeOut(500, function () {
            jQuery('.contact_us_page .form_responses_holder').fadeIn(500);
        });
    });
</script>
<?php
get_footer();
