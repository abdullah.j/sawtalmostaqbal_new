<?php
/**
 * Footer
 *
 * @package  TriTheme
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer  Marc bou sleiman <http://marcbousleiman.com>
 */
//wp_footer();
?>
<div class="footer">
    <h5>©جميع الحقوق محفوظة  <?php echo date('Y'); ?></h5>
<!--    <div class="container-fluid">
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 left_section">
            <?php
//            $instagram_link = get_field('instagram_link', 'option');
//            $youtube_link = get_field('youtube_link', 'option');
//            $soundcloud_link = get_field('soundcloud_link', 'option');
//            $facebook_link = get_field('facebook_link', 'option');
            ?>
            <ul class="social_media_wrapper">
                <li><a title="Facebook" class="social_media_btn" target="_blank" href="<?php // echo $facebook_link; ?>"><span class="social_spans"><i class="fa fa-facebook" aria-hidden="true"></i></span></a></li>
                <li><a title="Soundlcoud" class="social_media_btn" target="_blank" href="<?php // echo $soundcloud_link; ?>"><span class="social_spans"><i class="fa fa-soundcloud" aria-hidden="true"></i></span></a></li>
                <li><a title="Youtube" class="social_media_btn" target="_blank" href="<?php // echo $youtube_link; ?>"><span class="social_spans"><i class="fa fa-youtube-play" aria-hidden="true"></i></span></a></li>
                <li><a title="Instagram" class="social_media_btn" target="_blank" href="<?php // echo $instagram_link; ?>"><span class="social_spans"><i class="fa fa-instagram" aria-hidden="true"></i></span></a></li>
            </ul>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 right_section">
            <?php
//            wp_nav_menu(
//                    array(
//                        'theme_location' => 'main-menu',
//                        'menu_class' => 'main_top_menu',
//                        'walker' => new My_Custom_Nav_Walker()
//                    )
//            );
            ?>
        </div>
    </div>-->
</div>
</div>
<?php wp_footer(); ?>
</body>
</html>