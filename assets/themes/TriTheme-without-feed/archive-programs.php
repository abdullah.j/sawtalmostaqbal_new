<?php
/**
 * Archive programs page
 *
 * @package  TriTheme
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer  Marc bou sleiman <http://marcbousleiman.com>
 */
get_header();
?>
<div class="archive_programs_page padding_top">
    <h1 class="section_identifier">جدول البرامج</h1>
    <div class="container programs_wrapper">
        <div class="programs_to_infinite_scroll">
            <?php
            $paged1 = isset($_GET['paged1']) ? (int) $_GET['paged1'] : 1;
            $args = array(
                'post_type' => 'programs',
                'posts_per_page' => 6,
                'order' => 'asc',
                'post_status' => 'publish',
                'paged' => $paged1
            );

            $first = new WP_Query($args);

            if ($first->have_posts()):
                while ($first->have_posts()):
                    $first->the_post();
                    $program_time = get_field('program_time');
                    ?>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 each_program_wrapper">
                        <a href="<?php echo get_the_permalink(); ?>">
                            <div class="image_holder">
                                <div class="inner_image_holder">
                                    <?php echo the_post_thumbnail('homepage_programs', array('class' => 'each_program_img', 'alt' => get_the_title(), 'title' => get_the_title()));
                                    ?>
                                </div>
                                <h3><?php echo get_the_title(); ?></h3>
                                <div class="date_wrapper">
                                    <h4><?php echo $program_time; ?></h4>
                                </div>
                            </div>
                        </a>
                    </div>
                    <?php
                endwhile;
            endif;
            ?>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 get_more_btn">
                <?php
                $pag_args = array(
                    'format' => '?paged1=%#%',
                    'current' => $paged1,
                    'total' => $first->max_num_pages,
                    'next_text' => '<span>المزيد</span>',
                );
                echo paginate_links($pag_args);
                ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery('.programs_wrapper .programs_to_infinite_scroll').jscroll({
        loadingHtml: '<span class="loader"></span>',
        padding: 20,
        nextSelector: '.get_more_btn a.next',
        contentSelector: '.programs_wrapper .programs_to_infinite_scroll',
        autoTrigger: false
    });
</script>
<?php
get_footer();

