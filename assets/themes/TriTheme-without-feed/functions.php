<?php

/**
 * Functions
 *
 * @package  TriTheme
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer  Marc bou sleiman <http://marcbousleiman.com>
 */
/* -*****Backend functions start*****- */

/* Main start */
require_once(TEMPLATEPATH . '/includes/backend_functions/main/option-pages.php');
require_once(TEMPLATEPATH . '/includes/backend_functions/main/hide-pages.php');
/* Main end */

/* Posts start */
require_once(TEMPLATEPATH . '/includes/backend_functions/posts/posts.php');
require_once(TEMPLATEPATH . '/includes/backend_functions/posts/custom-posts.php');
require_once(TEMPLATEPATH . '/includes/backend_functions/posts/categories.php');
require_once(TEMPLATEPATH . '/includes/backend_functions/posts/custom-categories.php');
require_once(TEMPLATEPATH . '/includes/backend_functions/posts/custom-taxonomies.php');
require_once(TEMPLATEPATH . '/includes/backend_functions/posts/custom_cols.php');
/* Posts end */

/* -*****Backend functions end*****- */

/* -*****Functions start*****- */

/* AJAX FUNCTIONS CALLS start */
require_once(TEMPLATEPATH . '/includes/functions/ajax/sample-ajax.php');
/* AJAX FUNCTIONS CALLS end */

/* ACF API start */
require_once(TEMPLATEPATH . '/includes/functions/acf/select_populator.php');
require_once(TEMPLATEPATH . '/includes/functions/acf/disable_fields.php');
/* ACF API end */

/* Templates start */
require_once(TEMPLATEPATH . '/includes/templates/templates.php');
/* Templates end */

/* Templates start */
require_once(TEMPLATEPATH . '/includes/functions/enqueues/enqueues.php');
/* Templates end */

/* Core start */
require_once(TEMPLATEPATH . '/includes/functions/core/core.php');
/* Core end */

/* php libraries start */
require_once(TEMPLATEPATH . '/includes/functions/core/Mobile_Detect.php');
/* php libraries end */

/* -*****Functions end*****- */

if (!function_exists('TriTheme_setup')) :

    function TriTheme_setup() {
        add_theme_support('post-thumbnails');
//        set_post_thumbnail_size(600, 366, true);
        add_image_size('homepage_slider_prg_logo', 300, 250, true);
        add_image_size('homepage_programs', 615, 420, true);
        add_image_size('homepage_large_program_img', 800, 720, true);
        add_image_size('homepage_small_episode_img', 120, 120, true);
        add_image_size('single_program_top_img', 920, 490, true);
        add_image_size('single_program_episode_img', 390, 270, true);
        /**
         * Remove wpautop filter from content
         * (Remove <p> wrapping the text)
         */
        remove_filter('the_content', 'wpautop');
    }

endif;
add_action('after_setup_theme', 'TriTheme_setup');

/* add main menu */

function register_menus() {
    register_nav_menu('main-menu', __('Main Menu'));
}

add_action('init', 'register_menus');

/* Setting permalink structure */
add_action('init', function() {
    global $wp_rewrite;
    $wp_rewrite->set_permalink_structure('/%postname%/');
});
