<?php
/**
 * Core Functions
 *
 * @package  TriTheme
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer  _REPLACE_WITH_DEV_NAME_
 */

/**
 * arabic date translator
 *
 */
function single_post_arabic_date($postdate_d, $postdate_d2, $postdate_m, $postdate_y)
{

    $months = array("Jan" => "يناير", "Feb" => "فبراير", "Mar" => "مارس", "Apr" => "أبريل", "May" => "مايو", "Jun" => "يونيو", "Jul" => "يوليو", "Aug" => "أغسطس", "Sep" => "سبتمبر", "Oct" => "أكتوبر", "Nov" => "نوفمبر", "Dec" => "ديسمبر");
    $en_month = $postdate_m;
    foreach ($months as $en => $ar) {
        if ($en == $en_month) {
            $ar_month = $ar;
        }
    }
    $find = array("Sat", "Sun", "Mon", "Tue", "Wed", "Thu", "Fri");
    $replace = array("السبت", "الأحد", "الإثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة");
    $ar_day_format = $postdate_d2;
    $ar_day = str_replace($find, $replace, $ar_day_format);
    $post_date = $ar_day . ' ' . $postdate_d . ' ' . $ar_month . ' ' . $postdate_y;
    return $post_date;
}

/**
 * arabic time translator
 *
 */
function single_post_arabic_time($posttime_h, $posttime_i, $posttime_a)
{
    $post_time = $posttime_h . ':' . $posttime_i . " " . $posttime_a;
    return $post_time;
}

/**
 * limit excerpt words
 *
 */
function get_excerpt()
{
    $excerpt = get_the_content();
    $excerpt = preg_replace(" (\[.*?\])", '', $excerpt);
    $excerpt = strip_shortcodes($excerpt);
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, 160);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
    $excerpt = trim(preg_replace('/\s+/', ' ', $excerpt));
    return $excerpt;
}

/**
 * x limit excerpt words
 *
 */
function get_small_excerpt()
{
    $excerpt = get_the_content();
    $excerpt = preg_replace(" (\[.*?\])", '', $excerpt);
    $excerpt = strip_shortcodes($excerpt);
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, 100);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
    $excerpt = trim(preg_replace('/\s+/', ' ', $excerpt));
    return $excerpt;
}


/**
 * pagination for custom posts
 */
function TriTheme_pagination($paged = '', $pages = '')
{
    $return = "";
    if ($pages == '') {
        global $wp_query;
        $pages = ($wp_query->max_num_pages) ? $wp_query->max_num_pages : 1;
    }
    if ($pages != 1) {

        if ($paged < $pages) {
            $return .= "<a class='next-page' data-paged='$paged' data-pages='$pages' href=\"" . get_pagenum_link($paged + 1) . "\">المزيد</a>";
        }
        return $return;
    }
}

/* Add Next Page/Page Break Button in Visual Editor*/
add_filter('mce_buttons', 'my_add_next_page_button', 1, 2); // 1st row
function my_add_next_page_button($buttons, $id)
{

    /* only add this for content editor */
    if ('content' != $id)
        return $buttons;

    /* add next page after more tag button */
    array_splice($buttons, 13, 0, 'wp_page');

    return $buttons;
}

/*add unfiletered html capability to editor*/
add_filter('map_meta_cap', 'my_map_meta_cap', 1, 3);
function my_map_meta_cap($caps, $cap, $user_id)
{

    if ('unfiltered_html' === $cap && user_can($user_id, 'editor'))
        $caps = array('unfiltered_html');

    return $caps;
}


/*Limit SEO meta title to 50 char*/
function yoast_trim_title($str)
{
    if (mb_strlen($str) > 50) {
        $trimmer_str = mb_substr($str, 0, 50) . '...';
        return $trimmer_str;
    } else {
        return $str;
    }
}

add_filter('wpseo_title', 'yoast_trim_title');


//add_filter('query_vars', 'vod_vars');
//function vod_vars($qvars)
//{
//    $qvars[] = 'vod-element';
//    return $qvars;
//}


/*Detect Mobile*/
function is_mobile(){
    $useragent = $_SERVER['HTTP_USER_AGENT'];
    if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
        return $is_mobile = 1;
    } else {
        return $is_mobile = 0;
    }
}



/*custom excerpt*/
function custom_length($content, $count)
{
    if (strlen($content) > $count) {
        $count = $count - 22;
//        $content = substr($content, 0, $count) . '...';
        $content = mb_substr($content, 0, $count, 'utf-8') . '...';
        echo html_entity_decode($content);
    } else {
        echo html_entity_decode($content);
    }
}

/*Remove cloud tag styles*/
add_filter('wp_tag_cloud', 'tag_cloud_font_size_class');
function tag_cloud_font_size_class($taglinks)
{
    $tags = explode('</a>', $taglinks);
    $regex1 = "#(.*style='font-size:)(.*)((pt|px|em|pc|%);'.*)#e";
    $regex2 = "#(style='font-size:)(.*)((pt|px|em|pc|%);')#e";
    $regex3 = "#(.*class=')(.*)(' title.*)#e";
    foreach ($tags as $tag) {
        $size = preg_replace($regex1, "(''.round($2).'')", $tag); //get the rounded font size
        $tag = preg_replace($regex2, "('')", $tag); //remove the inline font-size style
        $tag = preg_replace($regex3, "('$1hvr-back-pulse '.($size).' $2$3')", $tag); //add .tag-size-{nr} class
        $tagn[] = $tag;
    }
    $taglinks = implode('</a>', $tagn);
    return $taglinks;
}


/*Menu fix for submenus*/
class My_Custom_Nav_Walker extends Walker_Nav_Menu {

    function start_lvl(&$output, $depth = 0, $args = array()) {
        $output .= "\n<ul class=\"dropdown-menu\">\n";
    }

    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
        $item_html = '';
        parent::start_el($item_html, $item, $depth, $args);

        if ( $item->is_dropdown && $depth === 0 ) {
            $item_html = str_replace( '<a', '<a class="dropdown-toggle" data-toggle="dropdown"', $item_html );
            $item_html = str_replace( '</a>', ' <b class="caret"></b></a>', $item_html );
        }

        $output .= $item_html;
    }

    function display_element($element, &$children_elements, $max_depth, $depth = 0, $args, &$output) {
        if ( $element->current )
            $element->classes[] = 'active';

        $element->is_dropdown = !empty( $children_elements[$element->ID] );

        if ( $element->is_dropdown ) {
            if ( $depth === 0 ) {
                $element->classes[] = 'dropdown';
            } elseif ( $depth === 1 ) {
                $element->classes[] = 'dropdown-submenu';
            }
        }

        parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
    }
}


// removing admin bar from all users
add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar() {
    if (is_user_logged_in()) {
        show_admin_bar(false);
    }
}

//limit contributor access to backend (API contributor)
function sawtalmoustakbal_no_admin_access() {
    if (is_admin() && !current_user_can('administrator') && !current_user_can('editor') && !(defined('DOING_AJAX') && DOING_AJAX)) {
        $redirect = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : home_url('/');
        global $current_user;
        $user_role = $current_user->roles;
        $disallowed_access = array(
            'subscriber',
            'contributor'
        );
        if (in_array($user_role[0], $disallowed_access)) {
            exit(wp_redirect($redirect));
        }
    }
}

add_action('admin_init', 'sawtalmoustakbal_no_admin_access', 100);

//adding filter to grant editor to see flamingo
remove_filter('map_meta_cap', 'flamingo_map_meta_cap');

add_filter('map_meta_cap', 'mycustom_flamingo_map_meta_cap', 9, 4);

function mycustom_flamingo_map_meta_cap($caps, $cap, $user_id, $args) {
    $meta_caps = array(
        'flamingo_edit_contact' => 'edit_posts',
        'flamingo_edit_contacts' => 'edit_posts',
        'flamingo_delete_contact' => 'edit_posts',
        'flamingo_edit_inbound_message' => 'publish_posts',
        'flamingo_edit_inbound_messages' => 'publish_posts',
        'flamingo_delete_inbound_message' => 'publish_posts',
        'flamingo_delete_inbound_messages' => 'publish_posts',
        'flamingo_spam_inbound_message' => 'publish_posts',
        'flamingo_unspam_inbound_message' => 'publish_posts',
        'flamingo_edit_outbound_message' => 'publish_posts',
        'flamingo_edit_outbound_messages' => 'publish_posts',
        'flamingo_delete_outbound_message' => 'publish_posts',
    );

    $caps = array_diff($caps, array_keys($meta_caps));

    if (isset($meta_caps[$cap]))
        $caps[] = $meta_caps[$cap];

    return $caps;
}