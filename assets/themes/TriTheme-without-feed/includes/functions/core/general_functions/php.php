<?php
/**
 * General php functions to be used !!SHOULD NOT BE ENQUEUED!!
 *
 * @package  TriTheme
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer  _REPLACE_WITH_DEV_NAME_
 */

/****************************************************************/
//general wp loop WITH TAX QUERY AND META QUERY
//$loop_args = array(
//    'posts_per_page' => _NUM_OF_POSTS_,
//    'post_type' => _POST_TYPE_NAME_,
//    'order' => _DESC/ASC_,
//    'post_status' => 'publish',
//    'tax_query' => array(
//        array(
//            'taxonomy' => _TAXONOMY_SLUG_HERE_,
//            'field' => _SLUG_OR_ID_,
//            'terms' => _TERM_NAME_,
//            operator => NOT IN_OR_NOT_OR_IN
//                    )
//                ),
//		'meta_query' => array(
//    array(
//        'key' => _ACF_KEY_SLUG_HERE_,
//        'compare' => '==',
//        'value' => _VALUE_HERE_
//    )
//),
//            )
//            $loop_query = new WP_Query($loop_args);
//
//            if $loop_query ->have_posts()) {
//                while $loop_query ->have_posts()) : $loop_query ->the_post();
//
//                endwhile; //end while
//            } //end if

/****************************************************************/

//loop for acf repeater rows

if (have_rows('_ACF_REPEATER_SLUG_HERE_')):
    while (have_rows('_ACF_REPEATER_SLUG_HERE_')) : the_row();
        $sub_variable = get_sub_field('_ACF_REPEATER_SUB_SLUG_HERE_');
        ?>
        <?php
    endwhile;
endif;

/****************************************************************/

// general acf functions
$variable = get_field('_ACF_FIELD_SLUG_HERE_');

/****************************************************************/

// adding favicon to the backend
function add_favicon() {
    $favicon_url = get_stylesheet_directory_uri() . '/images/favicon.ico';
    echo '<link rel="shortcut icon" href="' . $favicon_url . '" />';
}

add_action('login_head', 'add_favicon');
add_action('admin_head', 'add_favicon');

/****************************************************************/

/* acf maps api init*/
function my_acf_google_map_api($api) {

    $api['key'] = '_GOOGLE_MAPS_API_KEY_HERE';

    return $api;
}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

/****************************************************************/

/*
* Rename Categories to Brands
*/

function change_tax_object_label() {
    global $wp_taxonomies;
    $labels = &$wp_taxonomies['category']->labels;
    $labels->name = __('Brands', 'alaabi');
    $labels->singular_name = __('Brand', 'alaabi');
    $labels->search_items = __('Search Brands', 'alaabi');
    $labels->all_items = __('All Brands', 'alaabi');
    $labels->edit_item = __('Edit Brand', 'alaabi');
    $labels->view_item = __('View Brand', 'alaabi');
    $labels->update_item = __('Update Brand', 'alaabi');
    $labels->add_new_item = __('Add Brand', 'alaabi');
    $labels->new_item_name = __('Your Brand', 'alaabi');
}

add_action('init', 'change_tax_object_label');

/****************************************************************/


/*
 * Convert Arabic numbers to Hindi
 */

function arabic_w2e($str) {
    $arabic_eastern = array('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩', '`', '`', '-');
    $arabic_western = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '&#٨٢٢١;', '&#٨٢٢٠;', '&#٨٢١١;');
    return str_replace($arabic_western, $arabic_eastern, $str);
}

/****************************************************************/

/*
 * Quote inside content (news single)
 */

add_filter('the_content', 'insert_post_quote_html');

function insert_post_quote_html($content) {
    $post_code = '';
    $post_code .= '<div class="quote_holder">';
    $post_code .= '<p class="quote_icon_up">“</p>';
    $post_code .= '<p class="the_quote">' . get_field('quote') . '</p>';
    $post_code .= '<p class="quote_icon_down">„</p>';
    $post_code .= '</div>';
    if (is_single() && get_post_type() == 'post') {
        return insert_post_quote($post_code, 2, $content);
    }
    return $content;
}

function insert_post_quote($insertion, $paragraph_id, $content) {
    $closing_p = '</p>';
    $paragraphs = explode($closing_p, $content);
    foreach ($paragraphs as $index => $paragraph) {

        if (trim($paragraph)) {
            $paragraphs[$index] .= $closing_p;
        }

        if ($paragraph_id == $index + 1) {
            $paragraphs[$index] .= $insertion;
        }
    }

    return implode('', $paragraphs);
}

/****************************************************************/