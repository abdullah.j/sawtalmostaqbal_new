<?php
/**
 * Populate select independently
 *
 * @package  TriTheme
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer  _REPLACE_WITH_DEV_NAME_
 */
//add_action('init', 'acf_independent_select_populator');
//function acf_independent_select_populator()
//{
//    if (!is_admin()) {
//        return;
//    } else {
//        /* Insert years to selector */
//        function acf_load_years($field)
//        {
//            $i = 1;
//            for ($current_year = date('Y'); $current_year >= 1900; $current_year--) {
//                if($i == 1){
//                    $field['choices'][""] = "";
//                }else{
//                    $field['choices'][$current_year] = $current_year;
//                }
//                $i++;
//            }
//            return $field;
//        }
//
//        add_filter('acf/load_field/name=acf_select_field', 'acf_load_years');
//    }
//}