<?php
/**
 * Disable ACF fields
 *
 * @package  TriTheme
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer  _REPLACE_WITH_DEV_NAME_
 */
//if (!current_user_can('administrator')):
//    function acf_read_only_fields($field)
//    {
//        $field['disabled'] = true;
//        return $field;
//    }
//
//    add_filter('acf/load_field/name=acf_field_name', 'acf_read_only_fields');
//
//endif;