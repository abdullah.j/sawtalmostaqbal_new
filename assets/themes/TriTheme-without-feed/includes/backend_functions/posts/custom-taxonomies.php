<?php
/**
 * Custom Taxonomies Registration
 *
 * @package  TriTheme
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer  _REPLACE_WITH_DEV_NAME_
 */
//function sample_taxonomy_tags() {
//	$labels = array(
//		'name'                           => 'Sample Tags',
//		'singular_name'                  => 'Sample tag',
//		'search_items'                   => 'Search Sample Tags',
//		'all_items'                      => 'All Sample Tags',
//		'edit_item'                      => 'Edit Sample Tag',
//		'update_item'                    => 'Update Sample Tag',
//		'add_new_item'                   => 'Add New Sample Tag',
//		'new_item_name'                  => 'New Sample Tag',
//		'menu_name'                      => 'Sample Tags',
//		'view_item'                      => 'View Sample Tag',
//		'popular_items'                  => 'Popular Sample Tag',
//		'separate_items_with_commas'     => 'Separate Sample Tags with commas',
//		'add_or_remove_items'            => 'Add or remove Sample Tag',
//		'choose_from_most_used'          => 'Choose from the most used Sample Tags',
//		'not_found'                      => 'No Sample Tags found'
//	);
//
//	register_taxonomy(
//		'sample-tags',
//		'post_slug',
//		array(
//			'label' => __( 'Sample Tags' ),
//			'hierarchical' => false,
//			'labels' => $labels,
//			'public' => true,
//			'show_in_nav_menus' => false,
//			'show_tagcloud' => false,
//			'show_admin_column' => true,
//			'rewrite' => array(
//				'slug' => 'sample-tags'
//			)
//		)
//	);
//}
//add_action( 'init', 'sample_taxonomy_tags' );