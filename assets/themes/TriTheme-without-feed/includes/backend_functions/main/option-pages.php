<?php

/**
 * Option pages
 *
 * @package  TriTheme
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer  Marc bou sleiman <http://marcbousleiman.com>
 */
if (function_exists('acf_add_options_page')) {
    /**
     * meta option pages
     */
    acf_add_options_page(array(
        'page_title' => 'Homepage slider',
        'menu_title' => 'Homepage slider',
        'menu_slug' => 'homepage_slider',
        'capability' => 'edit_posts',
        'redirect' => false,
        'icon_url' => 'dashicons-image-filter',
    ));
    acf_add_options_page(array(
        'page_title' => 'Meta Tags',
        'menu_title' => 'Meta Tags',
        'menu_slug' => 'meta_tags',
        'capability' => 'edit_posts',
        'redirect' => false,
        'icon_url' => 'dashicons-admin-post',
    ));
    acf_add_options_page(array(
        'page_title' => 'About Us',
        'menu_title' => 'About Us',
        'menu_slug' => 'about-us',
        'capability' => 'edit_posts',
        'redirect' => false,
        'icon_url' => 'dashicons-groups',
    ));

    acf_add_options_page(array(
        'page_title' => 'Contact Info',
        'menu_title' => 'Contact Info',
        'menu_slug' => 'contact-info',
        'capability' => 'edit_posts',
        'redirect' => false,
        'icon_url' => 'dashicons-email-alt',
    ));
    acf_add_options_page(array(
        'page_title' => 'Privacy Policy',
        'menu_title' => 'Privacy Policy',
        'menu_slug' => 'privacy-policy',
        'capability' => 'edit_posts',
        'redirect' => false,
        'icon_url' => 'dashicons-email-alt',
    ));
    /**
     * ads option pages
     */
    acf_add_options_page(array(
        'page_title' => 'ADS',
        'menu_title' => 'ADS',
        'menu_slug' => 'ads-page',
        'capability' => 'edit_posts',
        'redirect' => false,
        'icon_url' => 'dashicons-welcome-add-page',
    ));

    acf_add_options_page(array(
        'page_title' => 'Analytics scripts',
        'menu_title' => 'Analytics scripts',
        'menu_slug' => 'analytics_scripts',
        'capability' => 'edit_posts',
        'redirect' => false,
        'icon_url' => 'dashicons-welcome-write-blog',
    ));

    acf_add_options_page(array(
        'page_title' => 'Mobile Ads Settings',
        'menu_title' => 'Mobile Ads Settings',
        'menu_slug' => 'mobile_ads_settings',
        'capability' => 'edit_posts',
        'redirect' => false,
        'icon_url' => 'dashicons-smartphone',
    ));
    
    acf_add_options_page(array(
        'page_title' => 'Radio.co ID',
        'menu_title' => 'Radio.co ID',
        'menu_slug' => 'radio_co_id',
        'capability' => 'edit_posts',
        'redirect' => false,
        'icon_url' => 'dashicons-smartphone',
    ));
}