<?php
/**
 * Header
 *
 * @package  TriTheme
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer  Marc bou sleiman <http://marcbousleiman.com>
 */
error_reporting(0);
ini_set("display_errors", "off");
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
    <!--<![endif]-->
    <head>

        <!-- Charset start -->
        <meta charset="<?php bloginfo('charset'); ?>">
        <!-- Charset end -->

        <!-- Mobile meta start -->
        <meta name="viewport" content="user-scalable=no,width=device-width,initial-scale=1.0,maximum-scale=1.0"/>
        <meta name="format-detection" content="telephone=no">
        <!-- Mobile meta end -->

        <!-- Profile and pingback meta start -->
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <!-- Profile and pingback meta end -->

        <!-- SEO meta start -->
        <?php
        if (has_sub_field('meta_tags_repeater', 'option')):
            ?>
            <meta name="keywords" content="
            <?php
            while (has_sub_field('meta_tags_repeater', 'option')):
                the_sub_field('meta_tags');
                ?> <?php
            endwhile;
            ?>">
                  <?php
              endif;
              ?>
        <!-- SEO meta end -->

        <!-- Title setting start -->
        <?php
        if (is_single()) {
            ?>
            <title itemprop="name"><?php the_title(); ?></title>

            <meta property="og:type" content="website"/>

            <!-- _Replace with Site Name_ -->
            <meta property="og:site_name" content="Sawt Al Mustaqbal"/>

            <!-- _Replace with Site URL -->
            <meta property="og:url" content="<?php echo get_site_url(); ?>"/>

            <!-- _Replace with Social Image (500px x 500px)_ -->
            <meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/images/social_login.png"/>

            <!-- _Replace with Site Name_ -->
            <meta property="og:title" content="Sawt Al Mustaqbal"/>

            <!-- _Replace with Site Desc_ -->
            <meta property="og:description"
                  content="Sawt Al Mustaqbal"/>

            <?php
        } else {
            ?>
            <title itemprop="name"><?php bloginfo('name') . is_front_page() ? '' : _e(' | ') . wp_title(''); ?></title>
            <?php
        }
        ?>
        <!-- Title setting end -->

        <!-- Browser header start -->

        <!-- Favicon start -->
        <!-- _Generate icon on http://realfavicongenerator.net/_ and place on /public_html -->
        <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192" href="/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/manifest.json">
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
        <link rel="icon" href="<?php echo site_url(); ?>/favicon.ico" type="image/x-icon">
        <link rel="shortcut icon" href="<?php echo site_url(); ?>/favicon.ico" type="image/x-icon">
        <!-- Favicon end -->

        <!-- Chrome, Firefox OS, Opera and Vivaldi-->

        <!-- _Replace with custom color_ -->
        <meta name="theme-color" content="#C92432">

        <!-- Windows Phone -->

        <!-- _Replace with custom color_ -->
        <meta name="msapplication-navbutton-color" content="#C92432">
        <meta name="msapplication-TileColor" content="#C92432">

        <!-- iOS Safari -->

        <!-- _Replace with custom color_ -->
        <meta name="apple-mobile-web-app-status-bar-style" content="#C92432">

        <!-- Browser header end -->

        <!-- Social Media Meta variables start -->
        <!-- _Replace with App ID_ -->
        <!--    <meta property="fb:app_id" content="_APP_ID_"/>-->

        <meta name="twitter:card" content="summary_large_image"/>

        <!-- _Replace with Site Name_ -->
        <meta name="twitter:title" content="Sawt Al Mustaqbal"/>

        <!-- _Replace with Site Desc_ -->
        <meta name="twitter:description"
              content="Sawt Al Mustaqbal"/>

        <!-- _Replace with Social Image (500px x 500px)_ -->
        <meta name="twitter:image" content="<?php echo get_template_directory_uri(); ?>/images/social_login.png"/>

        <!-- Social Media Meta variables end -->

        <!--IE fucking sucks start-->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!--IE fucking sucks end-->

        <?php wp_head(); ?>

		<!-- Facebook Pixel Code -->
        <script>
            !function (f, b, e, v, n, t, s)
            {
                if (f.fbq)
                    return;
                n = f.fbq = function () {
                    n.callMethod ?
                            n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq)
                    f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window, document, 'script',
                    'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '409721326505257');
            fbq('track', 'PageView');
        </script>
        <noscript>
    <img height="1" width="1"
         src="https://www.facebook.com/tr?id=409721326505257&ev=PageView
         &noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->
		
        <!-- Google Analytics Start -->
        <?php
        $google_analytics_script = get_field('google_analytics_script', 'option');
        echo $google_analytics_script;
        ?>
        <!-- Google Analytics End -->
    </head>

    <body <?php body_class(); ?> id="body">
        <div class="inner_body">
            <div class="top_header">
                <div class="container-fluid">
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 left_section">
                        <?php
                        $instagram_link = get_field('instagram_link', 'option');
                        $youtube_link = get_field('youtube_link', 'option');
                        $soundcloud_link = get_field('soundcloud_link', 'option');
                        $facebook_link = get_field('facebook_link', 'option');
                        ?>
                        <ul class="social_media_wrapper">
                            <li><a title="Facebook" class="social_media_btn" target="_blank" href="<?php echo $facebook_link; ?>"><span class="social_spans"><i class="fa fa-facebook" aria-hidden="true"></i></span></a></li>
                            <li><a title="Soundlcoud" class="social_media_btn" target="_blank" href="<?php echo $soundcloud_link; ?>"><span class="social_spans"><i class="fa fa-soundcloud" aria-hidden="true"></i></span></a></li>
                            <li><a title="Youtube" class="social_media_btn" target="_blank" href="<?php echo $youtube_link; ?>"><span class="social_spans"><i class="fa fa-youtube-play" aria-hidden="true"></i></span></a></li>
                            <!--<li><a title="Instagram" class="social_media_btn" target="_blank" href="<?php // echo $instagram_link; ?>"><span class="social_spans"><i class="fa fa-instagram" aria-hidden="true"></i></span></a></li>-->
                        </ul>
                        <div class="mobile_menu_bars mobile_menu_init">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/menu-bars.png" title="Open menu" alt="Open menu">
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 middle_section">
                        <?php
                        wp_nav_menu(
                                array(
                                    'theme_location' => 'main-menu',
                                    'menu_class' => 'main_top_menu',
                                    'walker' => new My_Custom_Nav_Walker()
                                )
                        );
                        ?>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 right_section">
                        <a title="Sawt al Mustaqbal" href="<?php echo esc_url(home_url('/')); ?>">
                            <img alt="Sawt al Mustaqbal" title="Sawt al Mustaqbal" src="<?php echo get_template_directory_uri(); ?>/images/logo.png">
                        </a>
                    </div>
                </div>
            </div>
            <div class="top_mobile_menu_section dont_close">
                <div class="top_mobile_part">
                    <span class="close_mobile_menu">
                        <img title="Back btn logo" alt="Back btn logo" class="x_red_logo" src="<?php echo get_template_directory_uri(); ?>/images/back-btn-white.png">
                    </span>
                    <a title="Sawt Al Mustaqbal" class="top_logo_wrapper" href="<?php echo esc_url(home_url('/')); ?>">
                        <img title="top logo" alt="top logo" class="top_logo" src="<?php echo get_template_directory_uri(); ?>/images/logo.png">
                    </a>
                    <div class="clear"></div>
                </div>
                <div class="mobile_menu_buttons">
                    <?php
                    wp_nav_menu(
                            array(
                                'theme_location' => 'main-menu',
                                'menu_class' => 'main_top_menu',
                                'walker' => new My_Custom_Nav_Walker()
                            )
                    );
                    ?>
                </div>
                <div class="social_media_wrapper">
                    <a title="Facebook" class="social_media_btn" target="_blank" href="<?php echo $facebook_link; ?>"><span class="social_spans"><i class="fa fa-facebook" aria-hidden="true"></i></span></a>
                    <a title="Soundcloud" class="social_media_btn" target="_blank" href="<?php echo $soundcloud_link; ?>"><span class="social_spans"><i class="fa fa-soundcloud" aria-hidden="true"></i></span></a>
                    <a title="Youtube" class="social_media_btn" target="_blank" href="<?php echo $youtube_link; ?>"><span class="social_spans"><i class="fa fa-youtube-play" aria-hidden="true"></i></span></a>
                    <!--<a title="Instagram" class="social_media_btn" target="_blank" href="<?php // echo $instagram_link; ?>"><span class="social_spans"><i class="fa fa-instagram" aria-hidden="true"></i></span></a>-->
                </div>
            </div>

