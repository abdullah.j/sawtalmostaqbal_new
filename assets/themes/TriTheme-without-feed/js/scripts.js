/*
 * Default scripts file
 *
 * @package  TriTheme
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer  Marc bou sleiman <http://marcbousleiman.com>
 */
jQuery(window).load(function () {
    jQuery('.homepage .poll_section .poll-holder .wp-polls-ans ul li').each(function () {
        var english_string = jQuery(this).find('small').html();
        if (english_string) {
            var new_string = english_string.replace("Votes", "أصوات");
            jQuery(this).find('small').html(new_string);
        }
    });
});
jQuery(document).ready(function ($) {
    if ((jQuery('body').hasClass('single-programs') || jQuery('body').hasClass('single-programs_episodes')) && (!jQuery('body').hasClass('postid-26') && !jQuery('body').hasClass('postid-1236'))) {
        jQuery('.top_header .middle_section ul li,.top_mobile_menu_section ul li').removeClass('active');
        jQuery('.top_header .middle_section ul li.menu-item-91,.top_mobile_menu_section ul li.menu-item-91').addClass('active');
    }
//on click of back logo go back
    function goBack() {
        window.history.back();
    }
    jQuery(document).on('click', '.back_logo', function () {
        goBack();
    });

//close open mobile menu button
    jQuery('.mobile_menu_init').on('click', function () {
        jQuery('.top_mobile_menu_section').fadeIn();
        jQuery('.top_header .right_section img').fadeOut();
        jQuery('.top_mobile_menu_section').animate({left: '0px'}, 500);
    });
    jQuery('.close_mobile_menu').on('click', function () {
        jQuery('.top_mobile_menu_section').animate({left: '-80%'}, 500);
        jQuery('.top_header .right_section img').fadeIn();
        jQuery('.top_mobile_menu_section').fadeOut();
    });

//    var myElement = document.getElementById('body');
//
//    var mc = new Hammer(myElement);
//// listen to events...
//    mc.on("panleft panright tap", function (ev) {
//        var delta_x_1 = ev['center'];
//        alert(delta_x_1);
//        var targeted = ev.target;
//        var targeted_classes = targeted['className'];
////        if(delta_x_2 > 0 && delta_x_2 < 100){
////            alert('wow');
////        }
//        
//        console.log(ev);
//        if (ev.type === 'panleft') {
//            jQuery('.mobile_right_menu_wrapper').addClass('opened');
//            jQuery('body').addClass('opened');
//        }
//        if (targeted_classes.indexOf("dont_close") <= 0) {
//            if (ev.type === 'tap' || ev.type === 'panright') {
//                jQuery('.mobile_right_menu_wrapper').removeClass('opened');
//                jQuery('body').removeClass('opened');
//            }
//        }
//    });
});